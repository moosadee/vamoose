package com.moosader.entities;

public class Effect extends EntityBase {
	/*
	public Sprite m_sprite;
	public TextureRegion m_region;
	public Texture m_texture;
	protected Vector2 m_coord;
	protected Vector2 m_dimen;
	*/
	
	protected float m_animateSpeed;
	protected int m_totalFrames;
	protected float m_frame;
	protected float m_size;
	protected boolean m_active;
	
	public void setup() {
		
	}
	
	protected void incrementFrame() {
		
	}
	
	protected void updateFrame() {
		incrementFrame();
		m_sprite.setRegion(m_region);
		m_sprite.setPosition(m_coord.x, m_coord.y);
		m_sprite.setSize(m_dimen.x, m_dimen.y);
	}
}

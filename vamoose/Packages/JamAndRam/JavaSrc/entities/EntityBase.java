// Pickin' Sticks Arcade, Rachel J. Morris 2012
// https://github.com/Moosader/Pickin-Sticks-Arcade
// www.moosader.com
// Licensed TBD!

package com.moosader.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class EntityBase {
	public Sprite m_sprite;
	public TextureRegion m_region;
	protected Vector2 m_coord;
	protected Vector2 m_dimen;
	
	public float getLeft() {
		return m_coord.x;
	}
	
	public float getRight() {
		return m_coord.x + m_dimen.x;
	}
	
	public float getTop() { 
		return m_coord.y + m_dimen.y;
	}
	
	public float getBottom() {
		return m_coord.y;
	}
	
	public float getWidth() {
		return m_dimen.x;
	}
	
	public float getHeight() {
		return m_dimen.y;
	}
}

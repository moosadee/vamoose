package com.moosader.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Bullet extends EntityBase {
	/*
	public Sprite m_sprite;
	public TextureRegion m_region;
	public Texture m_texture;
	
	protected Vector2 m_coord;
	protected Vector2 m_dimen;
	 */
	protected final float m_bulletSizeMin = 5f;
	protected final float m_bulletSizeMax = 15f;
	
	protected float m_maxSpeed;
	protected float m_accelerateSpeed;
	protected float m_speed;
	protected float m_initialSpeed;
	protected float m_bulletChargeRate;
	protected BulletState m_state;
	
	protected boolean m_isPlayerBullet;
	
	public Bullet() {
		m_dimen = new Vector2(m_bulletSizeMin, m_bulletSizeMin);
		m_coord = new Vector2(0, 0);
		m_sprite = new Sprite();
		m_maxSpeed = 300f;
		m_accelerateSpeed = 50f;
		m_speed = m_initialSpeed = 50f;
		m_state = BulletState.INACTIVE;
	}
	
	public Bullet(Bullet template) {
		m_dimen = new Vector2(template.m_dimen);
		m_coord = new Vector2(template.m_coord);
		m_sprite = new Sprite(template.m_sprite);
		m_accelerateSpeed = template.m_accelerateSpeed;
		m_isPlayerBullet = template.m_isPlayerBullet;
		m_maxSpeed = template.m_maxSpeed;
		m_speed = template.m_speed;
		m_state = template.m_state;
	}
	
	public void setupPlayerBullet(Sprite sprite) {
		reset();
		m_isPlayerBullet = true;
		m_sprite = sprite;
		m_bulletChargeRate = 0.2f;
	}
	
	public void setupEnemyBullet(Sprite sprite, float speed) {
		reset();	
		m_isPlayerBullet = false;	
		m_sprite = sprite;
		m_dimen.x = m_dimen.y = 10f;
		m_speed = -speed;
		m_initialSpeed = -speed;
	}
	
	public void reset() {
		m_dimen.x = m_bulletSizeMin;
		m_dimen.y = m_bulletSizeMin;
		//m_coord.x = m_coord.y = 0;
		m_speed = m_initialSpeed;
		m_state = BulletState.INACTIVE;
		updateFrame();
	}
	
	public void alignWithOwner(Vector2 ownerCoord, Vector2 ownerDimen) {
		m_coord.x = (m_isPlayerBullet) 
				? ownerCoord.x + ownerDimen.x - 48f		// Player coords
				: ownerCoord.x - 16f;					// Enemy coords
		m_coord.y = (ownerCoord.y + ownerDimen.y/3) - (m_dimen.y/2);
	}
	
	public void move(float delta) {
		m_speed += (m_accelerateSpeed / m_dimen.x / 5);
		if (m_isPlayerBullet) {
			m_coord.x += m_speed * delta;
		}
		else {
			m_coord.x -= m_speed * delta;			
		}
		checkBounds();
		updateFrame();
	}
	
	public BulletState getState() {
		return m_state;
	}
	
	public boolean isPlayerBullet() {
		return m_isPlayerBullet;
	}
	
	public float getSize() {
		return m_dimen.x;
	}
	
	public void shootingReleased() {
		if ( m_state == BulletState.CHARGING ) {
			m_state = BulletState.PROJECTILE;			
		}
		else {
			m_state = BulletState.INACTIVE;			
		}
	}
	
	public void shootingPressed() {
		m_state = BulletState.CHARGING;
	}
	
	public void setProjectile() {
		m_state = BulletState.PROJECTILE;
	}
	
	public void setInactive() {
		m_state = BulletState.INACTIVE;
		reset();
	}
	
	public void charge(Vector2 ownerCoord, Vector2 ownerDimen) {
		m_state = BulletState.CHARGING;
		
		m_dimen.x += m_bulletChargeRate;
		m_dimen.y += m_bulletChargeRate;
		
		alignWithOwner(ownerCoord, ownerDimen);

		checkBounds();
		updateFrame();
	}
	
	private void checkBounds() {
		// Size bounds
		if (m_dimen.x > m_bulletSizeMax) {
			m_dimen.x = m_dimen.y = m_bulletSizeMax;
		}
		if (m_dimen.x < m_bulletSizeMin) {
			m_dimen.x = m_dimen.y = m_bulletSizeMin;
		}
		
		// Acceleration bounds
		if (m_isPlayerBullet) {
			if (m_speed > m_maxSpeed) {
				m_speed = m_maxSpeed;
			}
		}
		else {
			m_dimen.x = m_dimen.y = 10f;
		}
		if (m_speed <= 0) {
			m_speed = m_initialSpeed;
		}
	}
	
	public Bullet release() {
		m_state = BulletState.PROJECTILE;
		m_accelerateSpeed = m_dimen.x/5 * 10;
		return this;
	}
	
	public void updateFrame() {
		m_sprite.setPosition(m_coord.x, m_coord.y);
		m_sprite.setSize(m_dimen.x, m_dimen.y);
	}
}



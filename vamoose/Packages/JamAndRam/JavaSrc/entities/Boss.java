package com.moosader.entities;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.moosader.jamram.GlobalOptions;
import com.moosader.managers.GraphicsManager;

public class Boss extends Character {
	protected final float m_liftSpeed = 10f;
	protected final float m_gravitySpeed = 5f;
	protected float m_verticalAcceleration;
	protected final float m_maxAcceleration = 4.0f;
	protected boolean m_buttonLiftPressed;
	protected Random m_moveChooser;
	protected int m_choiceCounter;
	protected int m_moveDir;
	protected int m_upMoves;
	protected int m_downMoves;
	
	public void setup() {
		super.setup();
		m_hp = m_totalHP = 70 * 30;
		m_coord = new Vector2(750f, 500f);
		m_dimen = new Vector2(200f, 150f);
		m_speed = new Vector2( 5f, 5 );
		m_verticalAcceleration = 0f;
		m_shootCooldown = 0f;
		m_shootCooldownMax = 20.0f;
		setupGraphics();
		m_chargingBullet.setupPlayerBullet(m_bulletSprite);
		m_buttonLiftPressed = false;
		m_moveChooser = new Random();
		m_choiceCounter = 0;
		m_moveDir = 0;
		m_upMoves = m_downMoves = 0;
		updateFrame();
	}
	
	protected void setupGraphics() {
		if ( GlobalOptions.isHalloween() ) {	
			m_dimen.x = 250f;
		}
		m_region = new TextureRegion(GraphicsManager.txBoss.texture, 0, 0, 
				(int)GraphicsManager.txBoss.frameW(), (int)GraphicsManager.txBoss.frameH());
		m_sprite = new Sprite(m_region);
		
		Sprite heartSprite = new Sprite(new TextureRegion(GraphicsManager.txHudHP.texture, 
				0, 0, GraphicsManager.txHudHP.frameW(), GraphicsManager.txHudHP.frameH()));
		heartSprite.setPosition(0, 0);
		heartSprite.setSize(32, 32);

		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
		m_lstHPSprites.add(new Sprite(heartSprite));
	}
	
	public void setMaxHP(int val) {
		m_hp = m_totalHP = val;
	}
	
	public void move(float delta) {
		if ( m_buttonLiftPressed ) {
			// Rise
			m_verticalAcceleration += m_liftSpeed * delta;
		}
		else {
			// Fall
			m_verticalAcceleration -= m_gravitySpeed * delta;
		}
		checkBounds();
		
		// Move player
		m_coord.y += m_verticalAcceleration;

		// Move hearts
		int hearts = m_hp / (m_totalHP/5);
		// Sanity check
		if ( hearts > m_lstHPSprites.size() ) {
			hearts = m_lstHPSprites.size();
		}
		for ( int index = hearts; index < m_lstHPSprites.size(); index++ ) {
			m_lstHPSprites.get(index).setPosition(m_lstHPSprites.get(index).getX(), m_lstHPSprites.get(index).getY() + 10);
		}
		for ( int index = 0; index < hearts; index++ ) {
			m_lstHPSprites.get(index).setPosition(m_coord.x + (index * 32), m_coord.y + m_dimen.y);
		}
	}
	
	public void handleMovement() {
		m_choiceCounter++;
		if ( m_choiceCounter % 10 == 0 ) {
			m_moveDir = m_moveChooser.nextInt(4);
			
			if ( m_upMoves / (m_downMoves+1) > 2 ) {
				m_moveDir = 1;
			}
			else if ( m_downMoves / (m_upMoves+1) > 2 ) {
				m_moveDir = 0;
			}
		}
		
		if ( m_moveDir == 0 ) {
			m_buttonLiftPressed = true;
			m_upMoves++;
			
		}
		else if ( m_moveDir == 1 ) {
			m_buttonLiftPressed = false;
			m_downMoves++;
		}
	}
	
	private void checkBounds() {
		if ( m_hp > 0 ) {
			// Acceleration bounds
			if (m_verticalAcceleration > m_maxAcceleration) {
				m_verticalAcceleration = m_maxAcceleration;
			}
			else if (m_verticalAcceleration < -m_maxAcceleration) {
				m_verticalAcceleration = -m_maxAcceleration;
			}
				
			// Position bounds
			if (m_coord.y > 1000 - m_dimen.y - 100) {
				// Off the top
				m_coord.y = 1000 - m_dimen.y - 100;
			}
			else if (m_coord.y < 0 + 100) {
				// Off the bottom
				m_coord.y = 100;
			}
			if (m_coord.x > 1000 - m_dimen.x ) {
				m_coord.x = 1000 - m_dimen.x;
			}
			else if (m_coord.x < 600) {
				m_coord.x = 600;
			}
		}
	}
	
	public boolean crash() {
		if ( m_coord.y > -m_dimen.y*3 ) {
			m_coord.y -= m_gravitySpeed;
			//m_coord.x += 5f;
			updateFrame();
			return true;
		}
		return false;
	}
}

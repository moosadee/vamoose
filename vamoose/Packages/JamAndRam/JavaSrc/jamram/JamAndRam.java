package com.moosader.jamram;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.moosader.managers.GraphicsManager;
import com.moosader.states.BaseState;
import com.moosader.states.BaseState.NextState;
import com.moosader.states.EndingState;
import com.moosader.states.GameState;
import com.moosader.states.MenuState;
import com.moosader.states.TitleState;

public class JamAndRam implements ApplicationListener {
	private BaseState currentScreen; 
	private GameState levelScreen1;
	private GameState levelScreen2;
	private TitleState titleScreen;
	private MenuState menuScreen;
	private EndingState endingScreen;
	
	private Music m_bgMusic;
	
	@Override
	public void create() {					
		GraphicsManager.setup();
		GraphicsManager.loadMenuImages();
		
		titleScreen = new TitleState();
		currentScreen = titleScreen;
	}

	@Override
	public void dispose() {
	}

	@Override
	public void render() {	
		currentScreen.update(Gdx.graphics.getDeltaTime());
		currentScreen.render(Gdx.graphics.getDeltaTime());
		
		if (Gdx.input.isKeyPressed(Keys.P)) {
			GlobalOptions.setIsHalloween(true);
		}
		if (Gdx.input.isKeyPressed(Keys.O)) {
			GlobalOptions.setIsHalloween(false);
		}
		
		if (currentScreen.isDone()) {
			changeState( currentScreen.m_nextState );	
		}
	}
	
	public void changeState(NextState state) {
		currentScreen.dispose();
		if ( state == NextState.MENU_SCREEN ) {
			if ( menuScreen == null ) {
				menuScreen = new MenuState();
			}
			GraphicsManager.loadMenuImages();
			currentScreen = menuScreen;
		}
		else if ( state == NextState.LEVEL1 ) { 
			if ( levelScreen1 == null ) {
				levelScreen1 = new GameState();
			}
			GraphicsManager.loadGameplayImages();
			levelScreen1.setup(1, menuScreen.isPilotHuman(), menuScreen.isGunnerHuman());
			currentScreen = levelScreen1;
		}
		else if ( state == NextState.LEVEL2 ) {
			if ( levelScreen2 == null ) {
				levelScreen2 = new GameState();
			}
			GraphicsManager.loadGameplayImages();
			levelScreen2.setup(2, menuScreen.isPilotHuman(), menuScreen.isGunnerHuman());
			currentScreen = levelScreen2;
		}
		else if ( state == NextState.YOU_WIN ) {
			if ( endingScreen == null ) {
				endingScreen = new EndingState();
			}
			GraphicsManager.loadMenuImages();
			endingScreen.setup();
			currentScreen = endingScreen;
		}
		currentScreen.reset();
		
		/*
		if (m_bgMusic != null) {
			m_bgMusic.stop();
			m_bgMusic.dispose();
		}
		if ( currentScreen.getMusicPath() != "" ) {
			System.out.println( "Play" );
			m_bgMusic = Gdx.audio.newMusic(Gdx.files.internal( currentScreen.getMusicPath() ));
			m_bgMusic.setLooping(true);
			m_bgMusic.stop();
			m_bgMusic.play();
		}
		*/
	}

	@Override
	public void resize(int width, int height) {
		currentScreen.resize(width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}

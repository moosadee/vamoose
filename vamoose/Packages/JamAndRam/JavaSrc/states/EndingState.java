package com.moosader.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.moosader.jamram.GlobalOptions;
import com.moosader.managers.GraphicsManager;

public class EndingState extends BaseState {
	private Sprite m_sprite;
	private SpriteBatch m_batch;
	private OrthographicCamera m_camera;
	
	public void setup() {
		m_batch = new SpriteBatch();
		m_sprite = new Sprite(new TextureRegion(GraphicsManager.txEndingSlide.texture, 
				0, 0, GraphicsManager.txEndingSlide.frameW(), GraphicsManager.txEndingSlide.frameH()));
		m_sprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		m_nextState = NextState.MENU_SCREEN;
		m_camera = new OrthographicCamera();
		m_camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		m_isDone = false;
	}

	@Override
	public void update(float delta) {
		if (!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.L) && Gdx.input.isKeyPressed(Keys.ANY_KEY)) {
			m_isDone = true;
		}
	}

	@Override
	public void draw(float delta) {	
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		m_batch.begin();
			m_sprite.draw(m_batch);
		m_batch.end();	
	}
	
	public String getMusicPath() {
		if ( GlobalOptions.isHalloween() ) {
			return "data/halloween/audio/LiftMotif_KevinMacLeod.mp3";			
		}
		else {
			return "data/audio/JustNasty_KevinMacLeod.mp3";			
		}
	}

	@Override
	public void reset() {
	}

	@Override
	public void resize(int width, int height) {
	}

}

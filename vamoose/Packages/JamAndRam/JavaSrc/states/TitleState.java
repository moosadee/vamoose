// Pickin' Sticks Arcade, Rachel J. Morris 2012
// https://github.com/Moosader/Pickin-Sticks-Arcade
// www.moosader.com
// Licensed TBD!

package com.moosader.states;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.moosader.jamram.GlobalOptions;
import com.moosader.managers.GraphicsManager;

public class TitleState extends BaseState {	
	public Sprite m_titleScreen;
	private SpriteBatch m_batch;
	private ArrayList<Sprite> m_lstSprites; 
	
	private Sprite m_titleSpr1;
	private Sprite m_titleSpr2;

	private Vector2 m_titleCoord1;
	private Vector2 m_titleCoord2;
	
	private float m_animTimer = 0.0f;
	private final float m_animSpeed = 1.0f;
	
	private final float m_panSpeed = 2.0f;
	
	private boolean m_nextScreen = false;
	
	public TitleState() { setup(); }
	
	public void reset() {
		setup();
	}
	
	public void setup() {
		m_nextState = NextState.MENU_SCREEN;
		m_isDone = false;
		m_lstSprites = new ArrayList<Sprite>();
		m_batch = new SpriteBatch();
		
		m_titleCoord1 = new Vector2(0, 360);
		m_titleCoord2 = new Vector2(0, -360 - m_panSpeed);

		m_titleSpr1 = new Sprite(new TextureRegion(GraphicsManager.txMenuTitlescreen1.texture,
				0, 0, GraphicsManager.txMenuTitlescreen1.frameW(), GraphicsManager.txMenuTitlescreen1.frameH()));
		m_titleSpr1.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());	
				
		m_titleSpr2 = new Sprite(new TextureRegion(GraphicsManager.txMenuTitlescreen2.texture,
				0, 0, GraphicsManager.txMenuTitlescreen2.frameW(), GraphicsManager.txMenuTitlescreen2.frameH()));
		m_titleSpr2.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());	
	}

	@Override
	public void update(float delta) {
		m_lstSprites.clear();
		
		if (Gdx.input.isKeyPressed(Keys.ANY_KEY) || Gdx.input.isTouched()) {
			m_nextScreen = true;
			m_animTimer = 0;
		}
		
		if (m_nextScreen == false && m_animTimer < 360/m_panSpeed) {
			m_titleCoord1.y -= m_panSpeed;
			m_titleCoord2.y += m_panSpeed;
			
			m_titleSpr1.setPosition(m_titleCoord1.x, m_titleCoord1.y);
			m_titleSpr2.setPosition(m_titleCoord2.x, m_titleCoord2.y);
		} 
		else if (m_nextScreen == true) {
			m_titleCoord1.y += m_panSpeed*2;
			m_titleCoord2.y -= m_panSpeed*2;
			
			m_titleSpr1.setPosition(m_titleCoord1.x, m_titleCoord1.y);
			m_titleSpr2.setPosition(m_titleCoord2.x, m_titleCoord2.y);			
		}
		
		if (m_nextScreen && m_animTimer > 360/m_panSpeed/2) {
			m_isDone = true;
		}
		
		m_lstSprites.add( m_titleSpr1 );
		m_lstSprites.add( m_titleSpr2 );
		
		m_animTimer += m_animSpeed;
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void draw(float delta) {		
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		m_batch.begin();
		for (Sprite s : m_lstSprites)
		{
			s.draw(m_batch);
		}
		m_batch.end();	
	}

}

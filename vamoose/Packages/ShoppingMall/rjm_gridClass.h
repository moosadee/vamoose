#ifndef _QA_GRIDCLASS_HPP
#define _QA_GRIDCLASS_HPP

#include "common.h"

#include <SFML/Graphics.hpp>

namespace ShoppingMall
{

class rjm_gridClass
{
    public:
        int codex;  //tileset x
        sf::Sprite image;
};

}

#endif

#ifndef _QA_NPCCLASS_HPP
#define _QA_NPCCLASS_HPP

#include "common.h"

#include <SFML/Graphics.hpp>

namespace ShoppingMall
{

class rjm_npcClass
{
    public:
    int x, y, w, h, talkx, talky, map;
//    SAMPLE *speak[2];
    int codex;
    void draw(sf::RenderWindow& window, sf::Sprite& image);
    bool talking;
    float talk_counter;
    rjm_npcClass() { talking = false; talk_counter = 0.0; w = 48; h = 96; }
    void increment_counter();
};

}

#endif

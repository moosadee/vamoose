#ifndef _QA_PLAYERCLASS_HPP
#define _QA_PLAYERCLASS_HPP

#include "rjm_bodyPartClass.h"
#include "common.h"

#include <string>

namespace ShoppingMall
{

class rjm_playerClass
{
    public:
        rjm_bodyPartClass body;
        rjm_bodyPartClass hair;
        rjm_bodyPartClass face;
        rjm_bodyPartClass clothes;
        rjm_bodyPartClass shoes;
        int x;
        int y;
        int w;
        int h;
        float frame;
        std::string direction;
        void setup( int player );
        void draw(sf::RenderWindow& window);
        void increment_frame();
        void move(std::string);
        void init_p2();
//        BITMAP *temp;
        float speed;
        void force_update();
        void swap(char);
};

}

#endif

#include "rjm_playerClass.h"

#include "../../chalo-engine/Managers/TextureManager.hpp"
#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Utilities/StringUtil.hpp"

namespace ShoppingMall
{

void rjm_playerClass::move(std::string dir)
{
    direction = dir;
    if ( dir == "up" && (y > 0))
    {
        y -= speed;
        body.y -= speed;
        clothes.y -= speed;
        face.y -= speed;
        shoes.y -= speed;
        hair.y -= speed;
    }
    else if ( dir == "down" && (y + h < 600) )
    {
        y += speed+.5;
        body.y += speed+.5;
        clothes.y += speed+.5;
        face.y += speed+.5;
        shoes.y += speed+.5;
        hair.y += speed+.5;
    }
    else if ( dir == "left" && (x > 0) )
    {
        x -= speed;
        body.x -= speed;
        clothes.x -= speed;
        face.x -= speed;
        shoes.x -= speed;
        hair.x -= speed;
    }
    else if ( dir == "right" && (x + w < 800) )
    {
        x += speed+.5;
        body.x += speed+.5;
        clothes.x += speed+.5;
        face.x += speed+.5;
        shoes.x += speed+.5;
        hair.x += speed+.5;
    }
}

void rjm_playerClass::increment_frame()
{
    frame += 0.08;
    if ( frame >= 2 ) { frame = 0; }
}

void rjm_playerClass::draw(sf::RenderWindow& window)
{
    int frame_i = (int)frame;

    sf::IntRect bodyFrame;
    sf::IntRect clothesFrame;
    sf::IntRect faceFrame;
    sf::IntRect shoesFrame;
    sf::IntRect hairFrame;
    bool flip = false;

    if ( direction == "left" )
    {
        bodyFrame = sf::IntRect( body.codex+(frame_i*body.w), body.codey+body.h, body.w, body.h );
        clothesFrame = sf::IntRect( clothes.codex, clothes.codey+(clothes.h*(frame_i+2)), clothes.w, clothes.h );
        faceFrame = sf::IntRect( face.codex, face.codey+(face.h*(frame_i+2)), face.w, face.h );
        shoesFrame = sf::IntRect( shoes.codex, shoes.codey+(shoes.h*(frame_i+2)), shoes.w, shoes.h );
        hairFrame = sf::IntRect( hair.codex, hair.codey+(hair.h*(frame_i+2)), hair.w, hair.h );
//        masked_blit(body.image, buffer, body.codex+(frame_i*body.w), body.codey+body.h, body.x, body.y, body.w, body.h);
//        masked_blit(clothes.image, buffer, clothes.codex, clothes.codey+(clothes.h*(frame_i+2)), clothes.x, clothes.y, clothes.w, clothes.h);
//        masked_blit(face.image, buffer, face.codex, face.codey+(face.h*(frame_i+2)), face.x+1, face.y, face.w, face.h);
//        masked_blit(shoes.image, buffer, shoes.codex, shoes.codey+(shoes.h*(frame_i+2)), shoes.x, shoes.y, shoes.w, shoes.h);
//        masked_blit(hair.image, buffer, hair.codex, hair.codey+(hair.h*(frame_i+2)), hair.x, hair.y, hair.w, hair.h);
    }
    else if ( direction == "up" )
    {
        bodyFrame = sf::IntRect( body.codex+(frame_i*body.w), body.codey+body.h*2, body.w, body.h );
        clothesFrame = sf::IntRect( clothes.codex, clothes.codey+(clothes.h*4)+(frame_i*clothes.h), clothes.w, clothes.h );
        faceFrame = sf::IntRect( 0, 0, face.w, face.h );
        shoesFrame = sf::IntRect( shoes.codex, shoes.codey+(shoes.h*4)+(frame_i*shoes.h), shoes.w, shoes.h );
        hairFrame = sf::IntRect( hair.codex, hair.codey+(hair.h*4)+(frame_i*hair.h), hair.w, hair.h );

//        masked_blit(body.image, buffer, body.codex+(frame_i*body.w), body.codey+body.h*2, body.x, body.y, body.w, body.h);
//        masked_blit(clothes.image, buffer, clothes.codex, clothes.codey+(clothes.h*4)+(frame_i*clothes.h), clothes.x, clothes.y, clothes.w, clothes.h);
//        masked_blit(hair.image, buffer, hair.codex, hair.codey+(hair.h*4)+(frame_i*hair.h), hair.x, hair.y, hair.w, hair.h);
//        masked_blit(shoes.image, buffer, shoes.codex, shoes.codey+(shoes.h*4)+(frame_i*shoes.h), shoes.x, shoes.y, shoes.w, shoes.h);
    }
    else if ( direction == "right" )
    {
        bodyFrame = sf::IntRect( body.codex+(frame_i*body.w), body.codey+body.h, body.w, body.h );
        clothesFrame = sf::IntRect( clothes.codex, clothes.codey+(clothes.h*(frame_i+2)), clothes.w, clothes.h );
        faceFrame = sf::IntRect( face.codex, face.codey+(face.h*(frame_i+2)), face.w, face.h );
        shoesFrame = sf::IntRect( shoes.codex, shoes.codey+(shoes.h*(frame_i+2)), shoes.w, shoes.h );
        hairFrame = sf::IntRect( hair.codex, hair.codey+(hair.h*(frame_i+2)), hair.w, hair.h );
        flip = true;
//        temp = create_bitmap(48, 96);
//        rectfill(temp, 0, 0, 48, 96, makecol(255, 0, 255));
//        masked_blit(body.image, temp, body.codex+(frame_i*body.w), body.codey+body.h, body.x-body.x, body.y-body.y, body.w, body.h);
//        masked_blit(clothes.image, temp, clothes.codex, clothes.codey+(clothes.h*(frame_i+2)), clothes.x-body.x, clothes.y-body.y, clothes.w, clothes.h);
//        masked_blit(face.image, temp, face.codex, face.codey+(face.h*(frame_i+2)), face.x+1-body.x, face.y-body.y, face.w, face.h);
//        masked_blit(shoes.image, temp, shoes.codex, shoes.codey+(shoes.h*(frame_i+2)), shoes.x-body.x, shoes.y-body.y, shoes.w, shoes.h);
//        masked_blit(hair.image, temp, hair.codex, hair.codey+(hair.h*(frame_i+2)), hair.x-body.x, hair.y-body.y, hair.w, hair.h);
//        draw_sprite_h_flip(buffer, temp, body.x, body.y);
    }
    else if ( direction == "down" )
    {
        bodyFrame = sf::IntRect( body.codex+(frame_i*body.w), body.codey, body.w, body.h );
        clothesFrame = sf::IntRect( clothes.codex, clothes.codey+(frame_i*clothes.h), clothes.w, clothes.h );
        faceFrame = sf::IntRect( face.codex, face.codey+(frame_i*face.h), face.w, face.h );
        shoesFrame = sf::IntRect( shoes.codex, shoes.codey+(frame_i*shoes.h), shoes.w, shoes.h );
        hairFrame = sf::IntRect( hair.codex, hair.codey+(frame_i*hair.h), hair.w, hair.h );
//        masked_blit(body.image, buffer, body.codex+(frame_i*body.w), body.codey, body.x, body.y, body.w, body.h);
//        masked_blit(clothes.image, buffer, clothes.codex, clothes.codey+(frame_i*clothes.h), clothes.x, clothes.y, clothes.w, clothes.h);
//        masked_blit(face.image, buffer, face.codex, face.codey+(frame_i*face.h), face.x+1, face.y, face.w, face.h);
//        masked_blit(hair.image, buffer, hair.codex, hair.codey+(frame_i*hair.h), hair.x, hair.y, hair.w, hair.h);
//        masked_blit(shoes.image, buffer, shoes.codex, shoes.codey+(frame_i*shoes.h), shoes.x, shoes.y, shoes.w, shoes.h);
    }


    if ( flip )
    {
        body.image.setOrigin( body.w, 0 );
        hair.image.setOrigin( hair.w, 0 );
        face.image.setOrigin( face.w, 0 );
        clothes.image.setOrigin( clothes.w, 0 );
        shoes.image.setOrigin( shoes.w, 0 );

        body.image.setScale( -1.0f, 1.0f );
        clothes.image.setScale( -1.0f, 1.0f );
        hair.image.setScale( -1.0f, 1.0f );
        face.image.setScale( -1.0f, 1.0f );
        shoes.image.setScale( -1.0f, 1.0f );
    }
    else
    {
        body.image.setOrigin( 0, 0 );
        hair.image.setOrigin( 0, 0 );
        face.image.setOrigin( 0, 0 );
        clothes.image.setOrigin( 0, 0 );
        shoes.image.setOrigin( 0, 0 );

        body.image.setScale( 1.0f, 1.0f );
        clothes.image.setScale( 1.0f, 1.0f );
        hair.image.setScale( 1.0f, 1.0f );
        shoes.image.setScale( 1.0f, 1.0f );
        face.image.setScale( 1.0f, 1.0f );
    }

    body.image.setPosition( sf::Vector2f( body.x, body.y ) );
    body.image.setTextureRect( bodyFrame );

    clothes.image.setPosition( clothes.x, clothes.y );
    clothes.image.setTextureRect( clothesFrame );

    hair.image.setPosition( hair.x, hair.y );
    hair.image.setTextureRect( hairFrame );

    shoes.image.setPosition( shoes.x, shoes.y );
    shoes.image.setTextureRect( shoesFrame );

    face.image.setPosition( face.x, face.y );
    face.image.setTextureRect( faceFrame );

    window.draw( body.image );
    window.draw( clothes.image );
    window.draw( face.image );
    window.draw( shoes.image );
    window.draw( hair.image );
}

void rjm_playerClass::setup( int player )
{
    speed = 3;
    x = 9*32;
    y = 10*32;
    w = 48;
    h = 96;
    direction = "down";
    body.codex = 96;
    body.codey = 0;
    body.image.setTexture( chalo::TextureManager::Get( "shopping_sprite_body" ) );
//    body.image = load_bitmap("shopping_sprite_body.bmp", NULL);

    hair.codex = 9*48;
    hair.codey = 0;
    hair.type = 2;
    hair.image.setTexture( chalo::TextureManager::Get( "shopping_sprite_hair" ) );
//    hair.image = load_bitmap("shopping_sprite_hair.bmp", NULL);

    face.codex = 48*3;
    face.codey = 0;
    face.type = 3;
    face.image.setTexture( chalo::TextureManager::Get( "shopping_sprite_makeup" ) );
//    face.image = load_bitmap("shopping_sprite_makeup.bmp", NULL);

    clothes.codex = 2*48;
    clothes.codey = 0;
    clothes.type = 2;
    clothes.image.setTexture( chalo::TextureManager::Get( "shopping_sprite_clothes" ) );
//    clothes.image = load_bitmap("shopping_sprite_clothes.bmp", NULL);

    shoes.codex = 5*48;
    shoes.codey = 0;
    shoes.type = 5;
    shoes.image.setTexture( chalo::TextureManager::Get( "shopping_sprite_shoes" ) );
//    shoes.image = load_bitmap("shopping_sprite_shoes.bmp", NULL);

    if ( player == 1 )
    {
        x = 13*32;
        y = 10*32;

        body.codex = 0;
        body.codey = 0;

        hair.codex = 9*48;
        hair.codey = 0;
        hair.type = 9;

        face.codex = 48*3;
        face.codey = 0;
        face.type = 3;

        clothes.codex = 3*48;
        clothes.codey = 0;
        clothes.type = 3;

        shoes.codex = 0;
        shoes.codey = 0;
        shoes.type = 0;
    }
    else if ( player == 2 )
    {
        x = 9*32;
        y = 12*32;

        body.codex = 0;
        body.codey = 0;

        hair.codex = 20*48;
        hair.codey = 0;
        hair.type = 9;

        face.codex = 48*3;
        face.codey = 0;
        face.type = 3;

        clothes.codex = 2*48;
        clothes.codey = 0;
        clothes.type = 3;

        shoes.codex = 0;
        shoes.codey = 0;
        shoes.type = 0;
    }
    else if ( player == 3 )
    {
        x = 13*32;
        y = 12*32;

        body.codex = 0;
        body.codey = 0;

        hair.codex = 15*48;
        hair.codey = 0;
        hair.type = 9;

        face.codex = 48*3;
        face.codey = 0;
        face.type = 3;

        clothes.codex = 4*48;
        clothes.codey = 0;
        clothes.type = 3;

        shoes.codex = 0;
        shoes.codey = 0;
        shoes.type = 0;
    }


    body.x = x;
    body.y = y;
    body.w = w;
    body.h = h;

    face.x = x;
    face.y = y+13;
    face.w = 48;
    face.h = 20;

    hair.x = x;
    hair.y = y;
    hair.w = 48;
    hair.h = 38;

    clothes.x = x;
    clothes.y = y+33;
    clothes.w = 48;
    clothes.h = 52;

    shoes.x = x;
    shoes.y = y+74;
    shoes.w = 48;
    shoes.h = 18;
}

void rjm_playerClass::force_update()
{
    body.x = x;
    body.y = y;
    body.w = w;
    body.h = h;

    hair.x = x;
    hair.y = y;
    hair.w = 48;
    hair.h = 38;

    face.x = x;
    face.y = y+13;
    face.w = 48;
    face.h = 20;

    clothes.x = x;
    clothes.y = y+33;
    clothes.w = 48;
    clothes.h = 52;

    shoes.x = x;
    shoes.y = y+74;
    shoes.w = 48;
    shoes.h = 18;
}

void rjm_playerClass::swap(char type)
{
    if ( type == 'h' )          //hair
    {
        hair.codex += 48;
        if ( hair.codex >= 1152 ) { hair.codex = 0; }
    }
    else if ( type == 's' )     //shoes
    {
        shoes.codex += 48;
        if ( shoes.codex >= 288 ) { shoes.codex = 0; }
    }
    else if ( type == 'c' )     //clothes
    {
        clothes.codex += 48;
        if ( clothes.codex >= 288 ) { clothes.codex = 0; }
    }
    else if ( type == 'm' )     //makeup
    {
        face.codex += 48;
        if ( face.codex >= 288 ) { face.codex = 0; }
    }
}

}

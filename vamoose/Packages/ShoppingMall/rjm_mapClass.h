#ifndef _QA_MAPCLASS_HPP
#define _QA_MAPCLASS_HPP

#include <string>
#include "rjm_gridClass.h"
#include "common.h"

#include <SFML/Graphics.hpp>

namespace ShoppingMall
{

class rjm_mapClass
{
    public:
        rjm_gridClass bottom_layer[max_x][max_y];
        rjm_gridClass top_layer[max_x][max_y];
        rjm_gridClass animated_layer[max_x][max_y];
        void draw(sf::RenderWindow& window, float);
        void draw_top(sf::RenderWindow& window, float);
        void load_map(std::string name);
        //warp areas
        int warp_x1[4];
        int warp_x2[4];
        int warp_y1[4];
        int warp_y2[4];
        int go_to[4];
        //entrance coordinates
        int enter_x[6];
        int enter_y[6];
};

}

#endif

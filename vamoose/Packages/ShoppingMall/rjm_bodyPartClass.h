#ifndef _QA_BODYPARTCLASS_HPP
#define _QA_BODYPARTCLASS_HPP

#include <string>
#include "common.h"

#include <SFML/Graphics.hpp>

namespace ShoppingMall
{

class rjm_bodyPartClass
{
    public:
        int x;
        int y;
        int w;
        int h;
        int codex;
        int codey;
        int type;
        sf::Sprite image;
};

}

#endif

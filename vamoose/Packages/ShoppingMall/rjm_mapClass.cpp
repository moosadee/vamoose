#include "rjm_mapClass.h"

#include <string>
#include <fstream>

#include "../../chalo-engine/Utilities/Logger.hpp"
#include "../../chalo-engine/Managers/TextureManager.hpp"

namespace ShoppingMall
{

void rjm_mapClass::load_map(std::string name)
{
    std::string filename;
    if (name == "mall_top")
    {
        filename = "Packages/ShoppingMall/content/shopping_malltop.map";
        //Clothes exit
        warp_x1[0] = 3 * 32;
        warp_x2[0] = 6 * 32;
        warp_y1[0] = 0 * 32 + 96;
        warp_y2[0] = 2 * 32 + 96;
        go_to[0] = 2;
        //Hairdo exit
        warp_x1[1] = 612;
        warp_x2[1] = 22 * 32;
        warp_y1[1] = 0 * 32 + 96;
        warp_y2[1] = 2 * 32 + 96;
        go_to[1] = 3;
        //Bottom exit
        warp_x1[2] = 10 * 32;
        warp_x2[2] = 14 * 32;
        warp_y1[2] = 17 * 32;
        warp_y2[2] = 18 * 32;
        go_to[2] = 0;
        //Excess
        warp_x1[3] = -1;
        warp_x2[3] = -1;
        warp_y1[3] = -1;
        warp_y2[3] = -1;
        go_to[3] = -2;
        enter_x[3] = -1;
        enter_y[3] = -1;
        //Entrances - from clothes, hair, or bottom of mall entrances
        //From bottom
        enter_x[0] = 326;
        enter_y[0] = 432;
        //from top
        enter_x[1] = -1;
        enter_y[1] = -1;
        //from clothes
        enter_x[2] = 92;
        enter_y[2] = 146;
        //from hair
        enter_x[3] = 600;
        enter_y[3] = 146;
        //from makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //from shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "mall_bottom")
    {
        filename = "Packages/ShoppingMall/content/shopping_mallbottom.map";
        //Top exit
        warp_x1[0] = 10 * 32;
        warp_x2[0] = 14 * 32;
        warp_y1[0] = 0;
        warp_y2[0] = 64;
        go_to[0] = 1;
        //Shoes exit
        warp_x1[1] = 4 * 32;
        warp_x2[1] = 7 * 32;
        warp_y1[1] = 4 * 32 + 96;
        warp_y2[1] = 6 * 32 + 96;
        go_to[1] = 5;
        //Makeup exit
        warp_x1[2] = 18 * 32;
        warp_x2[2] = 21 * 32;
        warp_y1[2] = 4 * 32 + 96;
        warp_y2[2] = 6 * 32 + 96;
        go_to[2] = 4;
        //Exit exit
        warp_x1[3] = 10 * 32;
        warp_x2[3] = 14 * 32;
        warp_y1[3] = 14 * 32 + 96;
        warp_y2[3] = 17 * 32 + 96;
        go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = 10*32;
        enter_y[0] = 13*32;
        //from mall top
        enter_x[1] = 321;
        enter_y[1] = 32;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = 575;
        enter_y[4] = 279;
        //From shoes
        enter_x[5] = 125;
        enter_y[5] = 279;
    }
    else if (name == "clothes")
    {
        filename = "Packages/ShoppingMall/content/shopping_clothes.map";
        warp_x1[0] = 10 * 32;
        warp_x2[0] = 14 * 32;
//        warp_y1[0] = 14 * 32 + 96;
//        warp_y2[0] = 17 * 32 + 96;
        warp_y1[0] = 550;
        warp_y2[0] = 600;
        go_to[0] = 1;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;

        //Entrances
        //from bottom
        enter_x[0] = -1;
        enter_y[0] = -1;
        //from mall top
        enter_x[1] = 320;
        enter_y[1] = 400;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "hair")
    {
        filename = "Packages/ShoppingMall/content/shopping_hair.map";
        //Bottom exit
        warp_x1[0] = 544;
        warp_x2[0] = 544+128;
//        warp_y1[0] = 14 * 32 + 96;
//        warp_y2[0] = 17 * 32 + 96;
        warp_y1[0] = 550;
        warp_y2[0] = 600;
        go_to[0] = 1;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = -1;
        enter_y[0] = -1;
        //from mall top
        enter_x[1] = 540;
        enter_y[1] = 450;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "shoes")
    {
        filename = "Packages/ShoppingMall/content/shopping_shoes.map";
        //Bottom exit
        warp_x1[0] = 320;
        warp_x2[0] = 320+160;
        warp_y1[0] = 14 * 32 + 96;
        warp_y2[0] = 17 * 32 + 96;
        go_to[0] = 0;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = 320;
        enter_y[0] = 428;
        //from mall tops
        enter_x[1] = -1;
        enter_y[1] = -1;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "makeup")
    {
        filename = "Packages/ShoppingMall/content/shopping_makeup.map";
        //Bottom exit
        warp_x1[0] = 96;
        warp_x2[0] = 96+128;
        warp_y1[0] = 14 * 32 + 96;
        warp_y2[0] = 17 * 32 + 96;
        go_to[0] = 0;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = 96;
        enter_y[0] = 422;
        //from mall top
        enter_x[1] = -1;
        enter_y[1] = -1;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }

    chalo::Logger::Out( "Loading map " + filename, "rjm_mapClass::load_map" );
    std::ifstream infile;
    infile.open(filename.c_str());
    if ( infile.fail() )
    {
        chalo::Logger::Error( "Failed to open map " + filename + "!", "rjm_mapClass::load_map" );
    }
    int tileWH = 32;
    for (int j=0; j<max_y; j++)
    {
        for (int i=0; i<max_x; i++)
        {
            int temp;
            infile>>temp;
            bottom_layer[i][j].codex = temp;
            bottom_layer[i][j].image.setTexture( chalo::TextureManager::Get( "shoppingTileset" ) );
            bottom_layer[i][j].image.setTextureRect( sf::IntRect( temp, 0, tileWH, tileWH ) );
            bottom_layer[i][j].image.setPosition( sf::Vector2f( i * tileWH, j * tileWH ) );
        }
    }
    //TOP LAYER
    for (int j=0; j<max_y; j++)
    {
        for (int i=0; i<max_x; i++)
        {
            int temp;
            infile>>temp;
            top_layer[i][j].codex = temp;
            top_layer[i][j].image.setTexture( chalo::TextureManager::Get( "shoppingTileset" ) );
            top_layer[i][j].image.setTextureRect( sf::IntRect( temp, 0, tileWH, tileWH ) );
            top_layer[i][j].image.setPosition( sf::Vector2f( i * tileWH, j * tileWH ) );
        }
    }
    //ANIMATED LAYER
    for (int j=0; j<max_y; j++)
    {
        for (int i=0; i<max_x; i++)
        {
            int temp;
            infile>>temp;
            animated_layer[i][j].codex = temp;
            animated_layer[i][j].image.setTexture( chalo::TextureManager::Get( "shoppingTileset" ) );
            animated_layer[i][j].image.setTextureRect( sf::IntRect( temp, 0, tileWH, tileWH ) );
            animated_layer[i][j].image.setPosition( sf::Vector2f( i * tileWH, j * tileWH ) );
        }
    }
    infile.close();
}

void rjm_mapClass::draw(sf::RenderWindow& window, float game_anim)
{
    //need to have it only draw what's on the screen
    for (int j=0; j<max_y; j++)
    {
        for (int i=0 ;i<max_x; i++)
        {
            window.draw( bottom_layer[i][j].image );
//                masked_blit(tileset, buffer, bottom_layer[i][j].codex, bottom_layer[i][j].codey, bottom_layer[i][j].x,
//                    bottom_layer[i][j].y, bottom_layer[i][j].w, bottom_layer[i][j].h);
                if ( top_layer[i][j].codex != 0 )      //if there is one
                {
                    window.draw( top_layer[i][j].image );
//                    masked_blit(tileset, buffer, top_layer[i][j].codex, top_layer[i][j].codey, top_layer[i][j].x,
//                        top_layer[i][j].y, top_layer[i][j].w, top_layer[i][j].h);
                }
                if ( animated_layer[i][j].codex != 0 && (int)game_anim == 1 )      //if there is one and at right frame
                {
                    window.draw( animated_layer[i][j].image );
//                    masked_blit(tileset, buffer, animated_layer[i][j].codex, animated_layer[i][j].codey, animated_layer[i][j].x,
//                        animated_layer[i][j].y, animated_layer[i][j].w, animated_layer[i][j].h);
                }
        }
    }
}

void rjm_mapClass::draw_top(sf::RenderWindow& window, float game_anim)
{
    for (int j=0; j<max_y; j++)
    {
        for (int i=0 ;i<max_x; i++)
        {
            if ( top_layer[i][j].codex != 0 &&
                    top_layer[i][j].codex == 95*32 || top_layer[i][j].codex == 96*32 ||
                    top_layer[i][j].codex == 97*32 )
            {
//                masked_blit(tileset, buffer, top_layer[i][j].codex, top_layer[i][j].codey, top_layer[i][j].x,
//                    top_layer[i][j].y, top_layer[i][j].w, top_layer[i][j].h);
            }
            if ( animated_layer[i][j].codex != 0 && (int)game_anim == 1 )      //if there is one and at right frame
            {
//                masked_blit(tileset, buffer, animated_layer[i][j].codex, animated_layer[i][j].codey, animated_layer[i][j].x,
//                    animated_layer[i][j].y, animated_layer[i][j].w, animated_layer[i][j].h);
            }
        }
    }
}

}

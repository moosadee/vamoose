#ifndef _QA_BUTTONCLASS_HPP
#define _QA_BUTTONCLASS_HPP

#include <string>
#include "common.h"
using namespace std;

namespace ShoppingMall
{

class buttonClass
{
    public:
        int x;
        int y;
        int w;
        int h;
        int grid_x;
        int grid_y;
        string go_to;
};

}

#endif

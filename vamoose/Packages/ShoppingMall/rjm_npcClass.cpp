#include "rjm_npcClass.h"

namespace ShoppingMall
{

void rjm_npcClass::draw(sf::RenderWindow& window, sf::Sprite& image)
{
    image.setPosition( x, y );
    sf::IntRect frame( codex, 0, w, h );
    image.setTextureRect( frame );
    window.draw( image );
//    masked_blit(image, buffer, codex, 0, x, y, w, h);
}

void rjm_npcClass::increment_counter()
{
    talk_counter += 1;
    if ( talk_counter >= 500 )
    {
        talk_counter = 0.0;
        talking = false;
    }
}

}

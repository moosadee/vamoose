#include "Character.h"

#include "../../chalo-engine/Utilities/Logger.hpp"

#include <math.h>

namespace RawrRinth
{

Character::Character()
{
    playerNumber = -1;
}

void Character::Setup( int player )
{
    playerNumber = player;

    if ( playerNumber == 0 )
    {
        x = 8*32;
        y = 36*32;
        w = 32;
        h = 32;
    }
    else if ( playerNumber == 1 )
    {
        x = 10*32;
        y = 36*32;
        w = 32;
        h = 32;
    }
    else if ( playerNumber == 2 )
    {
        x = 8*32;
        y = 38*32;
        w = 32;
        h = 32;
    }
    else if ( playerNumber == 3 || playerNumber == 5 )
    {
        x = 10*32;
        y = 38*32;
        w = 32;
        h = 32;
    }

    lastX = x;
    lastY = y;

    if ( player == 0 )
    {
        speed = 2.0f;
    }
    else
    {
        speed = 3.0f;
    }

    level = 1;
    frame = IDLE;
    direction = RIGHT;
    action = WALKING;
    FRAMEMAX = 4;
    region.X( x + 5 );
    region.Y( y + 5 );
    region.W( 24 );
    region.H( 24 );
    hp = 100;
    score = 0;
    atkCounter = -1;
    exists = true;
    fStrength = -0.5;
    szStrength = "-5";
    stamina = 1000;
    levelupExp = 40;
    powerup = 0;    //no power ups to begin with

    originalX = x;
    originalY = y;
    originalW = w;
    originalH = h;
    originalHP = hp;
}

void Character::Draw( sf::RenderWindow& window, sf::Sprite image, int offsetX, int offsetY )
{
    if ( exists )
    {
        if ( action != ATTACKING )
        {
            sf::IntRect textureRect( (int)frame * w, (int)direction * h, w, h );
            image.setPosition( x - offsetX, y - offsetY );
            image.setTextureRect( textureRect );
            window.draw( image );
//            masked_blit( source, destination, (int)frame * w, (int)direction * h, x - offsetX, y - offsetY, w, h );
        }
        else
        {
            int atk;
            if ( atkCounter > 5 ) { atk = 0; }
            else { atk = 1; }
            if ( direction == LEFT )
            {
                sf::IntRect textureRect( atk*w+(w*2), 2*h, w, h );
                image.setPosition( x - offsetX, y - offsetY );
                image.setTextureRect( textureRect );
                window.draw( image );
//                masked_blit( source, destination, atk*w+(w*2), 2*h, x - offsetX, y - offsetY, w, h );
            }
            else if ( direction == RIGHT )
            {
                sf::IntRect textureRect( atk*w, 2*h, w, h );
                image.setPosition( x - offsetX, y - offsetY );
                image.setTextureRect( textureRect );
                window.draw( image );
//                masked_blit( source, destination, atk*w, 2*h, x - offsetX, y - offsetY, w, h );
            }
        }
    }
}

void Character::SetDeadTimer()
{
    if ( deadTimer <= 0 )
    {
        deadTimer = 100;
    }
}

void Character::Reset( int x, int y )
{
    hp = originalHP;
    exists = true;

    this->x = x;
    this->y = y;

    levelupExp = 40;
    exp = 0;
    level = 1;
}

void Character::Update( int otherX, int otherY )
{
    if ( deadTimer > 0 )
    {
        deadTimer -= 0.1;
        cout << "Player dead timer: " << deadTimer << endl;

        if ( deadTimer <= 0 )
        {
            cout << "Respawn player" << endl;
            Reset( otherX, otherY );
        }
    }

    if ( !exists )
    {
        return;
    }

    region.X( x + 5 );
    region.Y( y + 5 );
    region.W( 24 );
    region.H( 24 );
    if ( atkCounter > 0 )
    {
        atkCounter -= 0.5;
    }
    else { atkCounter = -1; action = WALKING; }

    if ( playerNumber != 0 )
    {
        // Don't let player 2 leave player 1

        if ( x < otherX - 320 )
        {
            x = otherX - 320;
        }

        else if ( x > otherX + 320 - 32 )
        {
            x = otherX + 320 - 32;
        }

        if ( y < otherY - 240 )
        {
            y = otherY - 240;
        }

        else if ( y > otherY + 240 - 32 )
        {
            y = otherY + 240 - 32;
        }
    }


    lastX = x;
    lastY = y;
}

void Character::BeginAttack()
{
    if ( action != ATTACKING && atkCounter <= 0 )
    {
        action = ATTACKING;
        atkCounter = 10;
    }
}

void Character::Move( Direction dir )
{
    chalo::Logger::Out( "Player Move" );
    if ( !exists )
    {
        return;
    }

    if ( action != ATTACKING )
    {
        if ( dir == LEFT )
        {
            direction = dir;
            x = (int)(x - speed);
        }
        else if ( dir == RIGHT )
        {
            direction = dir;
            x = (int)(x + speed);
        }

        else if ( dir == UP )
        {
            y = (int)(y - speed);
        }
        else if ( dir == DOWN )
        {
            y = (int)(y + speed);
        }

        lastX = x;
        lastY = y;

        action = WALKING;
    }
    IncrementFrame();
}

void Character::IncrementFrame()
{
    int distX = lastX - x;
    int distY = lastY - y;

    int distance = sqrt( distX * distX + distY * distY );

    if ( !mousePlayer || distance > 1 )
    {
        frame += 0.15f;
        if ( frame >= FRAMEMAX )
            frame = 0.0f;
    }
}

void Character::Move( Direction dir, int otherX, int otherY )
{
    if ( !exists )
    {
        return;
    }

    if ( action != ATTACKING )
    {
        if ( dir == LEFT )
        {
            direction = dir;

            if ( x - speed < otherX - 320 )
            {
            }
            else
            {
                x = (int)(x - speed);
            }
        }
        else if ( dir == RIGHT )
        {
            direction = dir;

            if ( x + speed > otherX + 320 - 32 )
            {
            }
            else
            {
                x = (int)(x + speed);
            }
        }

        else if ( dir == UP )
        {

            if ( y - speed < otherY - 240 )
            {
            }
            else
            {
                y = (int)(y - speed);
            }
        }
        else if ( dir == DOWN )
        {

            if ( y + speed > otherY + 240 - 64 )
            {
            }
            else
            {
                y = (int)(y + speed);
            }
        }

        action = WALKING;
    }
    IncrementFrame();
}

}

#ifndef _RAWR_TILE_HPP
#define _RAWR_TILE_HPP

#include "Rect.h"

#include <SFML/Graphics.hpp>

namespace RawrRinth
{

class Tile
{
    public:
        sf::Sprite image;
        int x, y, w, h, fx, fy;
        Rect region;
        bool solid;
        Tile()
        {
            x = y = fx = fy = 0;
            w = h = 32;
            solid = false;
            region.X( 0 );
            region.Y( 0 );
            region.W( 32 );
            region.H( 32 );
        }
};

}

#endif

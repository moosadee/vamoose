#ifndef _QUIX_GAME_OBJECT
#define _QUIX_GAME_OBJECT

#include <SFML/Graphics.hpp>

class quixGameObject {
  private:
  bool enabled;
  public:
  sf::Sprite bmpBackground;
  bool getEnabled();
  void setEnabled(bool);
};

class MenuObject: public quixGameObject {
  private:
  bool done;
  int controlScheme[4];
  int fragLimit;
  int barrierScheme;
  public:
  sf::Sprite bmpWindow;
  sf::Sprite bmpPlay;
  sf::Sprite bmpPlayDepress;
  sf::Sprite bmpExit;
  sf::Sprite bmpExitDepress;
  sf::Sprite bmpCursor;
  sf::Sprite bmpControlScheme;
  sf::Sprite bmpArrows;
  sf::Sprite bmpScreenOpts;
  bool isDone();
  bool ExitPressed;
  bool PlayPressed;
  bool mouseDown;
  bool resetPressed;
  bool fragLimitHit;
  void quit();
  int returnControlScheme(int);
  void setControlScheme(int, int);
  int CheckClick(int, int);
  int xPlay, yPlay, xExit, yExit;
  MenuObject();
};

class LevelObject: public quixGameObject {
  private:
  int iFragLimit;
  int iBarrierScheme;
  int frame;
  int barrierX[20];
  int barrierY[20];
  int amountOfBarriers;
  int powerupType;
  int powerupFrame;
  int powerupX, powerupY;
  public:
  LevelObject();
  sf::Sprite bmpPlayerFilmstrip;
  sf::Sprite bmpLaserFilmstrip;

  int getFragLimit();
  void setFragLimit(int);
  int getBarrierScheme();
  void setBarrierScheme(int);
  int getFrame();
  void setFrame(int);

  int getBarrierX(int);
  int getBarrierY(int);
  void setBarrierCoordinates(int, int, int);

  int getBarrierAmount();
  void setBarrierAmount(int);

  void createPowerup();

  int timeForAPowerupTimer;
  //~LevelObject();
};

#endif

#include "Application/ChaloEngineProgram.hpp"
#include "chalo-engine/Utilities/StringUtil.hpp"
#include "chalo-engine/Utilities/Logger.hpp"

#include <cstdlib>
#include <ctime>
#include <string>

int main( int argc, char* argv[] )
{
    chalo::Logger::Setup();
    chalo::Logger::Out( "Total arguments: " + chalo::StringUtility::IntegerToString( argc ), "main()" );

    bool fullscreen = false;
    for ( int i = 0; i < argc; i++ )
    {
        std::string arg = std::string( argv[i] );
        chalo::Logger::Out( "- Argument " + chalo::StringUtility::IntegerToString( i ) + ": [" + arg + "]", "main()" );

        if ( arg == "--fullscreen" || arg == "-fullscreen" || arg == "fullscreen" )
        {
            fullscreen = true;
        }
    }

    fullscreen = true;

    chalo::Logger::Out( "Fullscreen: " + chalo::StringUtility::BooleanToString( fullscreen ), "main()" );

    srand((unsigned)time(NULL));
    ChaloEngineProgram program( fullscreen );
    program.Run();
}

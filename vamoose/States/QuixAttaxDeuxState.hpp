#ifndef _QUIX_ATTAX_DEUX_STATE
#define _QUIX_ATTAX_DEUX_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"
#include "../chalo-engine/GameObjects/Projectile.hpp"
#include "../chalo-engine/GameObjects/Tile.hpp"

#include "VamooseState.hpp"

#include <vector>

class QuixAttaxDeuxState : public VamooseState
{
public:
    QuixAttaxDeuxState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    void Shoot( int playerIndex );
    void ClearoutInactiveBullets();

    chalo::Character m_players[4];
    std::vector<chalo::Projectile> m_bullets;
    std::vector<chalo::Map::Tile> m_mapTiles;

    float scrollerX = -150.0, scrollerY = 5.0;

    int winner;

    int m_selectedCharacters[4];
    int m_bulletCooldown[4];
    const int BULLET_COOLDOWN_MAX;
    int m_fragOption;
    int m_barrierOption;
    bool m_isActive[4];
    bool m_isReady[4];
};

#endif

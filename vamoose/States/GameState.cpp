#include "GameState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"

GameState::GameState()
{
}

void GameState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "GameState::Init", "function-trace" );
    IState::Init( name );
}

void GameState::Setup()
{
    chalo::Logger::Out( "", "GameState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "character", "Content/Graphics/Characters/spritesheet-ayda-small.png" );

    chalo::InputManager::Setup();

    m_player.Setup();
    m_player.SetTexture( chalo::TextureManager::Get( "character" ), 0, 0, 40, 40 );
    m_player.SetPosition( 200, 200 );
    m_player.SetAnimationInformation( 4, 0.1 );
//    chalo::DrawManager::AddSprite( m_player.GetSprite() );
}

void GameState::Cleanup()
{
    chalo::Logger::Out( "", "GameState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
}

void GameState::Update()
{
    chalo::InputManager::Update();

    for ( int i = 0; i < 32; i++ )
    {
        if ( chalo::InputManager::IsJoystickButtonPressed( 0, i ) )
        {
            std::cout << i << std::endl;
        }
    }

    if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::A )
        || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::WEST ) )
    {
        m_player.Move( chalo::WEST );
    }
    else if (  chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::D )
            || chalo::InputManager::IsJoystickDpadPressed( 0, chalo::EAST ) )
    {
        m_player.Move( chalo::EAST );
    }

    if (    chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::W )
        ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::NORTH ) )
    {
        m_player.Move( chalo::NORTH );
    }
    else if (   chalo::InputManager::IsKeyPressedSmooth( sf::Keyboard::S )
            ||  chalo::InputManager::IsJoystickDpadPressed( 0, chalo::SOUTH ) )
    {
        m_player.Move( chalo::SOUTH );
    }

    m_player.Update();
}

void GameState::Draw( sf::RenderWindow& window )
{
    window.draw( m_player.GetSprite() );
//    chalo::DrawManager::Draw( window );
}




#ifndef _RAWR_RINTH_MENU_STATE
#define _RAWR_RINTH_MENU_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"

#include "VamooseState.hpp"

#include <vector>

namespace RawrRinth
{

class RawrRinthMenuState : public VamooseState
{
public:
    RawrRinthMenuState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    chalo::Character m_player;
};

}

#endif

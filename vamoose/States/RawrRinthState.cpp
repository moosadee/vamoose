#include "RawrRinthState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"


#include "../Packages/ShoppingMall/rjm_bodyPartClass.h"
#include "../Packages/ShoppingMall/rjm_buttonClass.h"
#include "../Packages/ShoppingMall/rjm_gridClass.h"
#include "../Packages/ShoppingMall/rjm_mapClass.h"
#include "../Packages/ShoppingMall/rjm_playerClass.h"
#include "../Packages/ShoppingMall/rjm_npcClass.h"
#include "../Packages/ShoppingMall/common.h"

namespace RawrRinth
{

RawrRinthGameState::RawrRinthGameState()
    : VamooseState()
{
}

void RawrRinthGameState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "RawrRinthState::Init", "function-trace" );
    VamooseState::Init( name );
}

void RawrRinthGameState::Setup()
{
    chalo::Logger::Out( "", "RawrRinthState::Setup", "function-trace" );
    VamooseState::Setup();

    // Which state comes before?
    m_backState = "startupstate";

    level[0].LoadMap( "Packages/RawrRinth/content/lv-00.map" );
    //Sounds
//    SAMPLE *sndPickup = load_sample( "content/ding.wav" );
//    SAMPLE *sndAttack = load_sample( "content/attack.wav" );
//    SAMPLE *sndDamage = load_sample( "content/explosion.wav" );
//    SAMPLE *sndConfirm = load_sample( "content/sfx2.wav" );
//    SAMPLE *sndDeath = load_sample( "content/death.wav" );
//    SAMPLE *sndLevelUp = load_sample( "content/levelup.wav" );
//    SAMPLE *sndBack = load_sample( "content/sfx.wav" );
//    SAMPLE *song1 = load_sample( "content/Dinosaur.wav" );

    chalo::TextureManager::Add( "gameover",     "Packages/RawrRinth/content/gameover.png" );
    chalo::TextureManager::Add( "title",        "Packages/RawrRinth/content/title.png" );
    chalo::TextureManager::Add( "items",        "Packages/RawrRinth/content/items.png" );
    chalo::TextureManager::Add( "coins",        "Packages/RawrRinth/content/coins.png" );
    chalo::TextureManager::Add( "egg",          "Packages/RawrRinth/content/egg.png" );
    chalo::TextureManager::Add( "icecream",     "Packages/RawrRinth/content/icecream.png" );
    chalo::TextureManager::Add( "cherry",       "Packages/RawrRinth/content/cherry.png" );
    chalo::TextureManager::Add( "bg",           "Packages/RawrRinth/content/bg.png" );
    chalo::TextureManager::Add( "kitty",        "Packages/RawrRinth/content/kitty.png" );
    chalo::TextureManager::Add( "snake",        "Packages/RawrRinth/content/snake.png" );
    chalo::TextureManager::Add( "ewok",         "Packages/RawrRinth/content/ewok.png" );
    chalo::TextureManager::Add( "robot",        "Packages/RawrRinth/content/robot.png" );
    chalo::TextureManager::Add( "player4",        "Packages/RawrRinth/content/player4.png" );
    chalo::TextureManager::Add( "player3",        "Packages/RawrRinth/content/player3.png" );
    chalo::TextureManager::Add( "player2",        "Packages/RawrRinth/content/player2.png" );
    chalo::TextureManager::Add( "rawr",         "Packages/RawrRinth/content/rawr.png" );

    // RasPi does not want to open an image of width 6400px
    chalo::TextureManager::Add( "tileset2",     "Packages/RawrRinth/content/tileset3.png" );

    imgTileset.setTexture( chalo::TextureManager::Get( "tileset2" ) );
    imgPlayer.setTexture( chalo::TextureManager::Get( "rawr" ) );
    imgPlayer2.setTexture( chalo::TextureManager::Get( "player2" ) );
    imgPlayer3.setTexture( chalo::TextureManager::Get( "player3" ) );
    imgPlayer4.setTexture( chalo::TextureManager::Get( "player4" ) );
    imgRobot.setTexture( chalo::TextureManager::Get( "robot" ) );
    imgEwok.setTexture( chalo::TextureManager::Get( "ewok" ) );
    imgSnake.setTexture( chalo::TextureManager::Get( "snake" ) );
    imgKitty.setTexture( chalo::TextureManager::Get( "kitty" ) );
    imgCherry.setTexture( chalo::TextureManager::Get( "cherry" ) );
    imgIcecream.setTexture( chalo::TextureManager::Get( "icecream" ) );
    imgEgg.setTexture( chalo::TextureManager::Get( "egg" ) );
    imgMoney.setTexture( chalo::TextureManager::Get( "coins" ) );
    imgHudItem.setTexture( chalo::TextureManager::Get( "items" ) );

    soundTimer2 = 0;
    soundTimer = 0;
    offsetX = 0;
    offsetY = 0;
    fruitCollected = 0;
    enemiesKilled = 0;
    gameTimer = 0.0;
    frame = 0;

    enemy[ level[0].EnemyCount() ];
    for ( int i=0; i<level[0].EnemyCount() ; i++ )      //Init enemies based on index
    {
        enemy[i].Setup( i );
    }
    item[ level[0].ItemCount() ];
    for ( int i=0; i<level[0].ItemCount(); i++ )        //Init items based on index
    {
        item[i].Setup( i );
    }

    playerCount = 4;
    for ( int i = 0; i < playerCount; i++ )
    {
        player[i].Setup( i );
    }
}

void RawrRinthGameState::Cleanup()
{
    chalo::Logger::Out( "", "RawrRinthState::Cleanup", "function-trace" );
    VamooseState::Cleanup();
}

void RawrRinthGameState::Update()
{
    VamooseState::Update();

    bool isMoving = false;

    //PLAYER MOVEMENT
    for ( int i = 0; i < playerCount; i++ )
    {
        int playerIndex = i;
        if ( i == 4 )
        {
            playerIndex = 0;
        }
        // Run
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_ACTION1 ) ) )
        {
            player[i].Speed( player[i].RUN() );
            player[i].DrainStamina();
        }
        else
        {
            player[i].Speed( player[i].WALK() );
            player[i].RestoreStamina();
        }

        // Attack
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_ACTION2 ) ) )
        {
            player[i].BeginAttack();
        }

        isMoving = false;
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_WEST ) ) )
        {
            isMoving = true;
            if ( IsCollision( &player[i], &level[0], LEFT) == false )
            {
                player[i].Move( LEFT );
            }
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_EAST ) ) )
        {
            isMoving = true;
            if ( IsCollision( &player[i], &level[0], RIGHT) == false )
            {
                player[i].Move( RIGHT );
            }
        }
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_NORTH ) ) )
        {
            isMoving = true;
            if ( IsCollision( &player[i], &level[0], UP) == false )
            {
                player[i].Move( UP );
            }
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_SOUTH ) ) )
        {
            isMoving = true;
            if ( IsCollision( &player[i], &level[0], DOWN) == false )
            {
                player[i].Move( DOWN );
            }
        }

        if ( isMoving )
        {
//            chalo::Logger::Out( "Player " + chalo::StringUtility::IntegerToString( playerIndex ) + " moving" );
        }
    }

    //Update enemies
    for ( int i=0; i<level[0].EnemyCount() ; i++ )
    {
        for ( int p = 0; p < playerCount; p++ )
        {
            if ( Distance( enemy[i].X(), enemy[i].Y(), player[p].X(), player[p].Y() ) <= VIEWDIST )
            {
                //chase player
                if ( (int)gameTimer % 3 == 0 )
                {
                    if ( player[p].Y() < enemy[i].Y() && IsCollision( &enemy[i], &level[0], UP ) == false )
                    {
                        enemy[i].Move( UP );
                    }
                    else if ( player[p].Y() > enemy[i].Y() && IsCollision( &enemy[i], &level[0], DOWN ) == false )
                    {
                        enemy[i].Move( DOWN );
                    }
                }
                else
                {
                    if ( player[p].X() < enemy[i].X() && IsCollision( &enemy[i], &level[0], LEFT ) == false )
                    {
                        enemy[i].Move( LEFT );
                    }
                    else if ( player[p].X() > enemy[i].X() && IsCollision( &enemy[i], &level[0], RIGHT ) == false )
                    {
                        enemy[i].Move( RIGHT );
                    }
                }
            }
        }
    }

    for ( int i = 0; i < playerCount; i++ )
    {
        player[i].Update( player[i].X(), player[i].Y() );
    }

    for ( int i=0; i<MaxText; i++ )       //update text animation
    {
        if ( txteffect[i].inUse )
        {
            txteffect[i].Update();
        }
    }

    //Check collisions
    for ( int i=0; i<level[0].ItemCount(); i++ )
    {
        if ( item[i].Exists() == false )
        {
            item[i].deadTimer -= 0.1;
            cout << "Item " << item[i].deadTimer << endl;
            if ( item[i].deadTimer <= 0 )
            {
                item[i].Exists( true );
                item[i].Reset();
                item[i].Setup( item[i].index );
                cout << "RESPAWN ITEM" << endl;
            }
        }

        for ( int p = 0; p < 2; p++ )
        {
            if ( item[i].Exists() == true && IsCollision( &player[p], &item[i] ) )
            {
                item[i].deadTimer = 100;
                item[i].Exists( false );
                player[p].AddScore( item[i].AdjustScore() );
                player[p].AddHP( item[i].AdjustHP() );
                player[p].AddMonies( item[i].AdjustMoney() );
//                play_sample( sndPickup, 255, 128, 1000, false );
                if ( item[i].Type() == CHERRY || item[i].Type() == ICECREAM )
                {
                    fruitCollected += 1;
                }
                else if ( item[i].Type() == EGG )
                {
                    player[p].AddEggs();
                }
                bool foundText = false;
                //Setup fancy text crap
                for ( int j=0; j<MaxText; j++ )
                {
                    if ( foundText == false )
                    {
                        if ( txteffect[j].inUse == false )
                        {
                            if ( item[i].Type() == CHERRY || item[i].Type() == ICECREAM )
                            {
                                txteffect[j].Setup( item[i].AdjustHPString().c_str(), item[i].X()+3, item[i].Y(), 100, 255, 75 );
                            }
                            else if ( item[i].Type() == MONEY )
                            {
                                txteffect[j].Setup( item[i].AdjustMoneyString().c_str(), item[i].X()+3, item[i].Y(), 255, 200, 0 );
                            }
                            else if ( item[i].Type() == EGG )
                            {
                                txteffect[j].Setup( item[i].AdjustEggString().c_str(), item[i].X()+3, item[i].Y(), 255, 255, 255 );
                            }
                            foundText = true;
                        }
                    }
                }
            }
        }
    }

    for ( int i=0; i<level[0].EnemyCount() ; i++ )
    {
        enemy[i].Update( player, gameTimer, soundTimer, soundTimer2, MaxText, txteffect );
    }

    //Check player stats
    bool allPlayersDead = true;
    for ( int i = 0; i < playerCount; i++ )
    {
        if ( player[i].HP() > 0 )
        {
            allPlayersDead = false;
        }
    }

    if ( allPlayersDead )
    {
        //game over
        game.State( sGAMEOVER );
    }
    else
    {
        for ( int i = 0; i < playerCount; i++ )
        {
            if ( player[i].HP() <= 0 )
            {
                player[i].Exists( false );
                player[i].SetDeadTimer();
            }
        }
    }

    if ( soundTimer > 0 )
    {
        soundTimer -= 0.5;
    }
    if ( soundTimer2 > 0 )
    {
        soundTimer2 -= 0.5;
    }
    gameTimer += 0.1;
    keyTimer -= 0.5;
    counter--;

    UpdateOffset( &offsetX, &offsetY, &player[0], &game );

    VamooseState::BackButtonHandler();
}

void RawrRinthGameState::Draw( sf::RenderWindow& window )
{

    game.BeginDraw();
    level[0].DrawBottomLayer( window, offsetX, offsetY, player[0].X(), player[0].Y() );
    //Items
    for ( int i=0; i<level[0].ItemCount(); i++ )
    {
        if ( item[i].Type() == CHERRY )
        {
            item[i].Draw( window, imgCherry, offsetX, offsetY );
        }
        else if ( item[i].Type() == ICECREAM )
        {
            item[i].Draw( window, imgIcecream, offsetX, offsetY );
        }
        else if ( item[i].Type() == MONEY )
        {
            item[i].Draw( window, imgMoney, offsetX, offsetY );
        }
        else if ( item[i].Type() == EGG )
        {
            item[i].Draw( window, imgEgg, offsetX, offsetY );
        }
        item[i].Update();
    }


    int r = 0, g = 0, b = 0;
    for ( int ty = -1; ty < 2; ty++ )
    {
        for ( int tx = -1; tx < 2; tx++ )
        {
//            circle( buffer, mouse_x + tx, mouse_y + ty, 16, makecol( r, g, b ) );
//            rect( buffer, mouse_x-1 + tx, mouse_y-1 + ty, mouse_x+1 + tx, mouse_y+1 + ty, makecol( r, g, b ) );
        }
    }

    // Mouse Circle
//    circle( buffer, mouse_x, mouse_y, 16, makecol( 0, 100, 255 ) );
//    rect( buffer, mouse_x-1, mouse_y-1, mouse_x+1, mouse_y+1, makecol( 0, 100, 255 ) );

    //Player
    player[0].Draw( window, imgPlayer, offsetX, offsetY );
    player[1].Draw( window, imgPlayer2, offsetX, offsetY );
    player[2].Draw( window, imgPlayer3, offsetX, offsetY );
    player[3].Draw( window, imgPlayer4, offsetX, offsetY );

    //Enemies
    for ( int i=0; i<level[0].EnemyCount() ; i++ )
    {
        if ( i % 4 == 0 )
        {
            enemy[i].Draw( window, imgEwok, offsetX, offsetY );
        }

        else if ( i % 4 == 1 )
        {
            enemy[i].Draw( window, imgRobot, offsetX, offsetY );
        }
        else if ( i % 4 == 2 )
        {
            enemy[i].Draw( window, imgKitty, offsetX, offsetY );
        }
        else
        {
            enemy[i].Draw( window, imgSnake, offsetX, offsetY );
        }
    }
    level[0].DrawTopLayer( window, offsetX, offsetY, player[0].X(), player[0].Y() );

    //basic HUD

    //Shadow
    int x, y;

    x = 150;
    y = 5;
    imgHudItem.setPosition( x, y );
    imgHudItem.setTextureRect( sf::IntRect( 0, 0, 32, 32  ) );
    window.draw( imgHudItem );

    x += 40;
    y += 32;
    int totalMoney = 0;
    for ( int i = 0; i < playerCount; i++ )
    {
        totalMoney += player[i].Monies();
    }
    lblMoney.Setup( "lblMoney", chalo::FontManager::Get( "main" ), 20, sf::Color::White, sf::Vector2f( x, y ), chalo::StringUtility::IntegerToString( totalMoney ) );
    window.draw( lblMoney.GetText() );

    x += 100;
    y = 5;
    imgHudItem.setPosition( x, y );
    imgHudItem.setTextureRect( sf::IntRect( 32, 0, 32, 32  ) );
    window.draw( imgHudItem );
    x += 40;
    y += 32;
    int totalEggs = 0;
    for ( int i = 0; i < playerCount; i++ )
    {
        totalEggs += player[i].Eggs();
    }
    lblEggs.Setup( "lblEggs", chalo::FontManager::Get( "main" ), 20, sf::Color::White, sf::Vector2f( x, y ), chalo::StringUtility::IntegerToString( totalEggs ) );
    window.draw( lblEggs.GetText() );

    x += 100;
    y = 5;
    imgHudItem.setPosition( x, y );
    imgHudItem.setTextureRect( sf::IntRect( 96, 0, 32, 32  ) );
    window.draw( imgHudItem );
    x += 40;
    y += 32;
    lblFruit.Setup( "lblFruit", chalo::FontManager::Get( "main" ), 20, sf::Color::White, sf::Vector2f( x, y ), chalo::StringUtility::IntegerToString( fruitCollected ) );
    window.draw( lblFruit.GetText() );

    x += 100;
    y = 5;
    imgHudItem.setPosition( x, y );
    imgHudItem.setTextureRect( sf::IntRect( 64, 0, 32, 32  ) );
    window.draw( imgHudItem );
    x += 40;
    y += 32;
    lblEnemies.Setup( "lblEnemies", chalo::FontManager::Get( "main" ), 20, sf::Color::White, sf::Vector2f( x, y ), chalo::StringUtility::IntegerToString( enemiesKilled ) );
    window.draw( lblEnemies.GetText() );

    // hp bars
    for ( int i = 0; i < playerCount; i++ )
    {
        if ( player[i].Exists() )
        {
            x = player[i].X() - offsetX - 7;
            y = player[i].Y() - offsetY - 25;

//        rectfill( buffer, x, y, x+(100/2), y+5, makecol( 20, 100, 20 ) );
            sf::RectangleShape hpbar( sf::Vector2f( 32, 6 ) );
            hpbar.setPosition( x, y );
            hpbar.setFillColor( sf::Color( 20, 20, 20 ) );
            hpbar.setOutlineColor( sf::Color( 0, 0, 0 ) );
            hpbar.setOutlineThickness( 1 );
            window.draw( hpbar );


//        rectfill( buffer, x, y, x+(player[0].HP()/2), y+5, makecol( 0, 255, 0 ) );
            sf::RectangleShape hpbar2( sf::Vector2f( ( player[i].HP() / 100 ) * 30, 4 ) );
            hpbar2.setPosition( x+1, y+1 );
            hpbar2.setFillColor( sf::Color( 20, 255, 20 ) );
            hpbar2.setOutlineColor( sf::Color( 0, 0, 0 ) );
            hpbar2.setOutlineThickness( 1 );
            window.draw( hpbar2 );

            // exp bar

            x = player[i].X() - offsetX - 7;
            y = player[i].Y() - offsetY - 15;
//
            sf::RectangleShape expbar( sf::Vector2f( 32, 6 ) );
            expbar.setPosition( x, y );
            expbar.setFillColor( sf::Color( 20, 20, 20 ) );
            expbar.setOutlineColor( sf::Color( 0, 0, 0 ) );
            expbar.setOutlineThickness( 1 );
            window.draw( expbar );

            sf::RectangleShape expbar2( sf::Vector2f( player[i].Exp() / player[i].ExpTilLevel() * 30, 4 ) );
            expbar2.setPosition( x+1, y+1 );
            expbar2.setFillColor( sf::Color( 240, 200, 20 ) );
            expbar2.setOutlineColor( sf::Color( 0, 0, 0 ) );
            expbar2.setOutlineThickness( 1 );
            window.draw( expbar2 );

//        rectfill( buffer, x, y, x+100/2, y+2, makecol( 25, 0, 0 ) );
//        rectfill( buffer, x, y, x+(((float)player[0].Exp()/(float)player[0].ExpTilLevel())*100)/2, y+2, makecol( 240, 200, 0 ) );
        }
    }

    x = 10;
    y = 10;

    chalo::UILabel label;
    label.Setup( "", chalo::FontManager::Get( "main" ), 15, sf::Color::White,
                 sf::Vector2f( x, y ), "PLAYER 1 LEVEL: " + chalo::StringUtility::IntegerToString( player[0].Level() ) );
    window.draw( label.GetText() );

    y += 20;
    label.Setup( "", chalo::FontManager::Get( "main" ), 15, sf::Color::White,
                 sf::Vector2f( x, y ), "PLAYER 2 LEVEL: " + chalo::StringUtility::IntegerToString( player[1].Level() ) );
    window.draw( label.GetText() );

    for ( int i=0; i<5; i++ )
    {
        if ( txteffect[i].inUse )
        {
            txteffect[i].Draw( window, offsetX, offsetY );
        }
    }

    if ( drawMiniMap )
    {
        DrawMiniMap( window, &level[0], &player[0], &player[1], &player[2], &player[3] );
    }

    game.EndDraw( window );

    VamooseState::Draw( window );
}


void RawrRinthGameState::UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game )
{
    if (    ( player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() ) >= 0 ) &&
            ( player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() ) <= 1920 ) )
    {
        *offsetX = player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() );
    }
    if (    ( player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() ) >= 0 ) &&
            ( player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() ) <= 1438 ) )
    {
        *offsetY = player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() );
    }
}

bool RawrRinthGameState::IsCollision( Character *character, Level *level, int dir )
{
    Rect player, tile[8];
    bool solid[8];
    player.x = character->RX();
    player.y = character->RY();
    player.w = character->RW();
    player.h = character->RH();

    if ( dir == UP )
    {
        player.y -= character->Speed();
    }
    else if ( dir == DOWN )
    {
        player.y += character->Speed();
    }
    else if ( dir == LEFT )
    {
        player.x -= character->Speed();
    }
    else if ( dir == RIGHT )
    {
        player.y += character->Speed();
    }

    //Check four surrounding tiles, on bottom and middle layers (top layer is ABOVE player, no solid ones)
    //layer 0, top-left tile
    //Set(x, y, w, h)
    tile[0].Set( level->tile[0][ player.x / 32 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 ].y,
                 level->tile[0][ player.x / 32 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 ].h );
    solid[0] = level->tile[0][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 0, top-right tile
    tile[1].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].y,
                 level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[1] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 0, bottom-left tile
    tile[2].Set( level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].y,
                 level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[2] = level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 0, bottom-right tile
    tile[3].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                 level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[3] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    //layer 1, top-left tile
    tile[4].Set( level->tile[1][ player.x / 32 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 ].y,
                 level->tile[1][ player.x / 32 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 ].h );
    solid[4] = level->tile[1][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 1, top-right tile
    tile[5].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].y,
                 level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[5] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 1, bottom-left tile
    tile[6].Set( level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].y,
                 level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[6] = level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 1, bottom-right tile
    tile[7].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                 level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[7] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    for ( int i=0; i<8; i++ )
    {
        if ( solid[i] == true && IsCollision( player, tile[i] ) )
            return true;
    }

    return false;
}

bool RawrRinthGameState::IsCollision( Enemy *character, Level *level, int dir )
{
    Rect player, tile[8];
    bool solid[8];
    player.x = character->RX();
    player.y = character->RY();
    player.w = character->RW();
    player.h = character->RH();

    if ( dir == UP )
    {
        player.y -= character->Speed();
    }
    else if ( dir == DOWN )
    {
        player.y += character->Speed();
    }
    else if ( dir == LEFT )
    {
        player.x -= character->Speed();
    }
    else if ( dir == RIGHT )
    {
        player.y += character->Speed();
    }

    //Check four surrounding tiles, on bottom and middle layers (top layer is ABOVE player, no solid ones)
    //layer 0, top-left tile
    //Set(x, y, w, h)
    tile[0].Set( level->tile[0][ player.x / 32 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 ].y,
                 level->tile[0][ player.x / 32 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 ].h );
    solid[0] = level->tile[0][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 0, top-right tile
    tile[1].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].y,
                 level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[1] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 0, bottom-left tile
    tile[2].Set( level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].y,
                 level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[2] = level->tile[0][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 0, bottom-right tile
    tile[3].Set( level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                 level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[3] = level->tile[0][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    //layer 1, top-left tile
    tile[4].Set( level->tile[1][ player.x / 32 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 ].y,
                 level->tile[1][ player.x / 32 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 ].h );
    solid[4] = level->tile[1][ player.x / 32 ][ player.y / 32 ].solid;
    //layer 1, top-right tile
    tile[5].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].y,
                 level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].h );
    solid[5] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 ].solid;
    //layer 1, bottom-left tile
    tile[6].Set( level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].y,
                 level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].h );
    solid[6] = level->tile[1][ player.x / 32 ][ player.y / 32 + 1 ].solid;
    //layer 1, bottom-right tile
    tile[7].Set( level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].x, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].y,
                 level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].w, level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].h );
    solid[7] = level->tile[1][ player.x / 32 + 1 ][ player.y / 32 + 1 ].solid;

    for ( int i=0; i<8; i++ )
    {
        if ( solid[i] == true && IsCollision( player, tile[i] ) )
            return true;
    }

    return false;
}

bool RawrRinthGameState::IsCollision( Character *player, Item *item )
{
    Rect pl, it;
    pl.x = player->RX();
    pl.y = player->RY();
    pl.w = player->RW();
    pl.h = player->RH();

    it.x = item->RX();
    it.y = item->RY();
    it.w = item->RW();
    it.h= item->RH();

    return IsCollision( pl, it );
}

bool RawrRinthGameState::IsCollision( Character *player, Enemy *enemy )
{
    Rect pl, en;
    pl.x = player->RX();
    pl.y = player->RY();
    pl.w = player->RW();
    pl.h = player->RH();

    en.x = enemy->RX();
    en.y = enemy->RY();
    en.w = enemy->RW();
    en.h= enemy->RH();

    return IsCollision( pl, en );
}

void RawrRinthGameState::IncrementCounter()
{
    counter++;
}

bool RawrRinthGameState::IsCollision( Rect r1, Rect r2 )
{
    if (    r1.x            <       r2.x+r2.w &&
            r1.x+r1.w    >       r2.x &&
            r1.y            <       r2.y+r2.h &&
            r1.y+r1.h     >       r2.y )
    {
        return true;
    }
    return false;
}

float RawrRinthGameState::Distance( int x1, int y1, int x2, int y2 )
{
    //a^2 + b^2 = c^2
    return ( sqrt ( (x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) ) );
}

void RawrRinthGameState::DrawMenuText( sf::RenderWindow& window )
{
    int x = 520, y = 220;

    //shadow
//    textprintf_centre_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//    textprintf_centre_ex( buffer, font, x, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//    textprintf_centre_ex( buffer, font, x+1, y+1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//
//    textprintf_centre_ex( buffer, font, x-1, y, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//    textprintf_centre_ex( buffer, font, x+1, y, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//
//    textprintf_centre_ex( buffer, font, x-1, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//    textprintf_centre_ex( buffer, font, x, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//    textprintf_centre_ex( buffer, font, x+1, y-1, makecol( 0, 0, 0 ), -1, "Press ENTER to start" );
//
//    //gold text
//    textprintf_centre_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "Press ENTER to start" );

    y = 230;

    //shadow
//    textprintf_centre_ex( buffer, font, x-1, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//    textprintf_centre_ex( buffer, font, x, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//    textprintf_centre_ex( buffer, font, x+1, y+1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//
//    textprintf_centre_ex( buffer, font, x-1, y, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//    textprintf_centre_ex( buffer, font, x+1, y, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//
//    textprintf_centre_ex( buffer, font, x-1, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//    textprintf_centre_ex( buffer, font, x, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//    textprintf_centre_ex( buffer, font, x+1, y-1, makecol( 0, 0, 0 ), -1, "or F4 to quit" );
//
//    //gold text
//    textprintf_centre_ex( buffer, font, x, y, makecol( 255, 200, 0 ), -1, "or F4 to quit" );
}

void RawrRinthGameState::DrawMiniMap( sf::RenderWindow& window, Level *level, Character *player, Character *player2, Character *player3, Character *player4 )
{
    return; // TODO: Find faster way to do this

    int x = 8;
    int y = 650;
    sf::RectangleShape pixel( sf::Vector2f( 1.0f, 1.0f ) );
    for ( int i=0; i<TILEX; i++ )
    {
        for ( int j=0; j<TILEY; j++ )
        {
            if ( level->tile[0][i][j].solid )
            {
                pixel.setFillColor( sf::Color( 255, 255, 255 ) );
                pixel.setPosition( i+x, j+y );
                window.draw( pixel );
//                putpixel( buffer, i+x, j+y, makecol( 255, 255, 255 ) );
            }
            if ( level->tile[1][i][j].solid )
            {
                pixel.setFillColor( sf::Color( 255, 255, 0 ) );
                pixel.setPosition( i+x, j+y );
                window.draw( pixel );
//                putpixel( buffer, i+x, j+y, makecol( 255, 255, 0 ) );
            }

            pixel.setFillColor( sf::Color( 255, 0, 0 ) );
            pixel.setPosition( player->X() / 32 + x+1, player->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player->X() / 32 + x, player->Y() / 32 + y+1 );
            window.draw( pixel );
            pixel.setPosition( player->X() / 32 + x-1, player->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player->X() / 32 + x, player->Y() / 32 + y-1 );
            window.draw( pixel );
            pixel.setPosition( player->X() / 32 + x, player->Y() / 32 + y );
            window.draw( pixel );

            pixel.setFillColor( sf::Color( 0, 0, 255 ) );
            pixel.setPosition( player2->X() / 32 + x+1, player2->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player2->X() / 32 + x, player2->Y() / 32 + y+1 );
            window.draw( pixel );
            pixel.setPosition( player2->X() / 32 + x-1, player2->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player2->X() / 32 + x, player2->Y() / 32 + y-1 );
            window.draw( pixel );
            pixel.setPosition( player2->X() / 32 + x, player2->Y() / 32 + y );
            window.draw( pixel );

            pixel.setFillColor( sf::Color( 255, 0, 255 ) );
            pixel.setPosition( player3->X() / 32 + x+1, player3->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player3->X() / 32 + x, player3->Y() / 32 + y+1 );
            window.draw( pixel );
            pixel.setPosition( player3->X() / 32 + x-1, player3->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player3->X() / 32 + x, player3->Y() / 32 + y-1 );
            window.draw( pixel );
            pixel.setPosition( player3->X() / 32 + x, player3->Y() / 32 + y );
            window.draw( pixel );

            pixel.setFillColor( sf::Color( 0, 255, 255 ) );
            pixel.setPosition( player4->X() / 32 + x+1, player4->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player4->X() / 32 + x, player4->Y() / 32 + y+1 );
            window.draw( pixel );
            pixel.setPosition( player4->X() / 32 + x-1, player4->Y() / 32 + y );
            window.draw( pixel );
            pixel.setPosition( player4->X() / 32 + x, player4->Y() / 32 + y-1 );
            window.draw( pixel );
            pixel.setPosition( player4->X() / 32 + x, player4->Y() / 32 + y );
            window.draw( pixel );

//            putpixel( buffer, (player->X()/32) + x,      (player->Y()/32) + y-1, makecol( 255, 0, 0 ) );//up
//            putpixel( buffer, (player->X()/32) + x,      (player->Y()/32) + y+1, makecol( 255, 0, 0 ) );//down
//            putpixel( buffer, (player->X()/32) + x-1,    (player->Y()/32) + y, makecol( 255, 0, 0 ) );//left
//            putpixel( buffer, (player->X()/32) + x+1,    (player->Y()/32) + y, makecol( 255, 0, 0 ) );//right
//            putpixel( buffer, (player->X()/32) + x,      (player->Y()/32) + y, makecol( 255, 0, 0 ) );//center
//
//            putpixel( buffer, (player2->X()/32) + x,      (player2->Y()/32) + y-1, makecol( 0, 0, 255 ) );//up
//            putpixel( buffer, (player2->X()/32) + x,      (player2->Y()/32) + y+1, makecol( 0, 0, 255 ) );//down
//            putpixel( buffer, (player2->X()/32) + x-1,    (player2->Y()/32) + y, makecol( 0, 0, 255 ) );//left
//            putpixel( buffer, (player2->X()/32) + x+1,    (player2->Y()/32) + y, makecol( 0, 0, 255 ) );//right
//            putpixel( buffer, (player2->X()/32) + x,      (player2->Y()/32) + y, makecol( 0, 0, 255 ) );//center

        }
    }
}

}

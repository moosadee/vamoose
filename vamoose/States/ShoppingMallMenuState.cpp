#include "ShoppingMallMenuState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"

#include "VamooseState.hpp"

namespace ShoppingMall
{

ShoppingMallMenuState::ShoppingMallMenuState()
    : VamooseState()
{
}

void ShoppingMallMenuState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "ShoppingMallMenuState::Init", "function-trace" );
    VamooseState::Init( name );
}

void ShoppingMallMenuState::Setup()
{
    chalo::Logger::Out( "", "ShoppingMallMenuState::Setup", "function-trace" );
    VamooseState::Setup();

    m_backState = "startupstate";

    chalo::TextureManager::Add( "character", "Content/Graphics/Characters/spritesheet-ayda-small.png" );

    chalo::InputManager::Setup();

    chalo::MenuManager::LoadTextMenu( "shoppingmall.chalomenu" );
}

void ShoppingMallMenuState::Cleanup()
{
    chalo::Logger::Out( "", "ShoppingMallMenuState::Cleanup", "function-trace" );
    VamooseState::Cleanup();
}

void ShoppingMallMenuState::Update()
{
    VamooseState::Update();
    VamooseState::BackButtonHandler();
}

void ShoppingMallMenuState::Draw( sf::RenderWindow& window )
{
    VamooseState::Draw( window );
}

}


#include "BootlegBashMenuState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"

#include "VamooseState.hpp"

BootlegBashMenuState::BootlegBashMenuState()
    : VamooseState()
{
}

void BootlegBashMenuState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "BootlegBashMenuState::Init", "function-trace" );
    VamooseState::Init( name, true );
}

void BootlegBashMenuState::Setup()
{
    chalo::Logger::Out( "", "BootlegBashMenuState::Setup", "function-trace" );
    VamooseState::Setup();

    m_backState = "startupstate";

    chalo::TextureManager::Add( "sidebar-left", "Packages/BootlegBash/content/sidebar-left.png" );
    chalo::TextureManager::Add( "sidebar-right", "Packages/BootlegBash/content/sidebar-right.png" );
    chalo::TextureManager::Add( "portraits", "Packages/BootlegBash/content/portraits.png" );
    chalo::TextureManager::Add( "bgboxes", "Packages/BootlegBash/content/bgboxes.png" );
    chalo::TextureManager::Add( "topbar", "Packages/BootlegBash/content/topbar.png" );

    chalo::InputManager::Setup();

    chalo::MenuManager::LoadTextMenu( "bootlegbash.chalomenu" );
}

void BootlegBashMenuState::Cleanup()
{
    chalo::Logger::Out( "", "BootlegBashMenuState::Cleanup", "function-trace" );
    VamooseState::Cleanup();
}

void BootlegBashMenuState::Update()
{
    VamooseState::Update();
    VamooseState::BackButtonHandler();
}

void BootlegBashMenuState::Draw( sf::RenderWindow& window )
{
    VamooseState::Draw( window );
}




#ifndef _VAMOOSE_STATE
#define _VAMOOSE_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"

#include <string>

class VamooseState : public chalo::IState
{
    public:
    VamooseState();

    virtual void Init( const std::string& name, bool useCursors = false );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    protected:
    void BackButtonHandler();
    int m_backTimer;
    sf::Sprite m_adminBlep;
    std::string m_backState;

    int m_logTimer;

    void CursorSetup();
    void CursorUpdate();
    void CursorDraw();
    std::vector<chalo::Character> m_cursors;
    bool m_useCursors;
};


#endif

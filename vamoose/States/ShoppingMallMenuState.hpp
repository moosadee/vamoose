#ifndef _SHOPPING_MALL_MENU_STATE
#define _SHOPPING_MALL_MENU_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"

#include "VamooseState.hpp"

#include <vector>

namespace ShoppingMall
{

class ShoppingMallMenuState : public VamooseState
{
public:
    ShoppingMallMenuState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    chalo::Character m_player;
};

}

#endif

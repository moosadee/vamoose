#include "RawrRinthMenuState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"

#include "VamooseState.hpp"

namespace RawrRinth
{

RawrRinthMenuState::RawrRinthMenuState()
    : VamooseState()
{
}

void RawrRinthMenuState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "RawrRinthMenuState::Init", "function-trace" );
    VamooseState::Init( name );
}

void RawrRinthMenuState::Setup()
{
    chalo::Logger::Out( "", "RawrRinthMenuState::Setup", "function-trace" );
    VamooseState::Setup();

    m_backState = "startupstate";

    chalo::TextureManager::Add( "sidebar-left", "Packages/RawrRinth/content/sidebar-left.png" );
    chalo::TextureManager::Add( "sidebar-right", "Packages/RawrRinth/content/sidebar-right.png" );
    chalo::TextureManager::Add( "selectbg", "Packages/RawrRinth/content/selectbg.png" );
    chalo::TextureManager::Add( "characterselect", "Packages/RawrRinth/content/characterselect.png" );

    chalo::InputManager::Setup();

    chalo::MenuManager::LoadTextMenu( "rawrrinth.chalomenu" );
}

void RawrRinthMenuState::Cleanup()
{
    chalo::Logger::Out( "", "RawrRinthMenuState::Cleanup", "function-trace" );
    VamooseState::Cleanup();
}

void RawrRinthMenuState::Update()
{
    VamooseState::Update();
    VamooseState::BackButtonHandler();
}

void RawrRinthMenuState::Draw( sf::RenderWindow& window )
{
    VamooseState::Draw( window );
}

}

#ifndef _SHOPPING_MALL_STATE
#define _SHOPPING_MALL_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"
#include "../chalo-engine/GameObjects/Projectile.hpp"
#include "../chalo-engine/GameObjects/Tile.hpp"

#include "VamooseState.hpp"

#include <vector>

#include "../Packages/ShoppingMall/rjm_bodyPartClass.h"
#include "../Packages/ShoppingMall/rjm_buttonClass.h"
#include "../Packages/ShoppingMall/rjm_gridClass.h"
#include "../Packages/ShoppingMall/rjm_mapClass.h"
#include "../Packages/ShoppingMall/rjm_playerClass.h"
#include "../Packages/ShoppingMall/rjm_npcClass.h"
#include "../Packages/ShoppingMall/common.h"

namespace ShoppingMall
{

class ShoppingMallState : public VamooseState
{
public:
    ShoppingMallState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    rjm_mapClass map[6];
    int map_index = 0;
    int totalPlayers;
    rjm_playerClass player[4];
    rjm_npcClass clerk[6];
    bool two_player;
    buttonClass button[btn_amt];
    float game_anim;
    float counter;
    sf::Sprite tileset;
    sf::Sprite clerkimg;

    void init_buttons(buttonClass [btn_amt]);
    void check_click(buttonClass button, int mouse_x, int mouse_y, int index, bool *done, bool *, bool *);
    int check_movement(rjm_playerClass[2], rjm_mapClass, bool two_player);
    void go_to_map(rjm_mapClass[], rjm_playerClass[], int, int*);
    void move_player( rjm_playerClass*, rjm_mapClass, string, float, rjm_npcClass clerk[6], int map_index );
    void init_clerks( rjm_npcClass[] );
    void check_clerk_collision( rjm_playerClass player[2], rjm_npcClass clerk[6], int map_index );
};

}

#endif

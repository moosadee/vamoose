#include "StartupState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"

StartupState::StartupState()
    : VamooseState()
{
}

void StartupState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "StartupState::Init", "function-trace" );
    VamooseState::Init( name, true );
}

void StartupState::Setup()
{
    chalo::Logger::Out( "", "StartupState::Setup", "function-trace" );
    VamooseState::Setup();

    chalo::TextureManager::Add( "button-long",              "Content/Graphics/UI/button-long.png" );
    chalo::TextureManager::Add( "button-long-selected",     "Content/Graphics/UI/button-long-selected.png" );
    chalo::TextureManager::Add( "button-square",            "Content/Graphics/UI/button-square.png" );
    chalo::TextureManager::Add( "button-square-selected",   "Content/Graphics/UI/button-square-selected.png" );

    chalo::TextureManager::Add( "button-quixattaxdeux",     "Content/Graphics/UI/button-quixattaxdeux.png" );
    chalo::TextureManager::Add( "button-bootlegbash",       "Content/Graphics/UI/button-bootlegbash.png" );
    chalo::TextureManager::Add( "button-rawrrinth",         "Content/Graphics/UI/button-rawrrinth.png" );
    chalo::TextureManager::Add( "button-shoppingmall",      "Content/Graphics/UI/button-shoppingmall.png" );
    chalo::TextureManager::Add( "bg",                       "Content/Graphics/UI/background.png" );

    chalo::MenuManager::LoadTextMenu( "startup.chalomenu" );

    sf::Vector2f pos( 360, 700 );
    sf::IntRect dimensions( 0, 0, 300, 35 );
}

void StartupState::Cleanup()
{
    chalo::Logger::Out( "", "StartupState::Cleanup", "function-trace" );
    VamooseState::Cleanup();
}

void StartupState::Update()
{
    VamooseState::Update();

    // Player 1 can navigate elseware
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) )
    {
        std::string clickedButton = chalo::MenuManager::GetHoveredButton( m_cursors[0].GetPosition() );

        if      ( clickedButton == "btnQuixAttax" )     { SetGotoState( "quixattaxdeuxstate" ); } //{ SetGotoState( "quixattaxdeuxmenustate" ); }
        else if ( clickedButton == "btnBootlegBash" )   { SetGotoState( "bootlegbashmenustate" ); }
        else if ( clickedButton == "btnShoppingMall" )  { SetGotoState( "shoppingmallstate" ); } //{ SetGotoState( "shoppingmallmenustate" ); }
        else if ( clickedButton == "btnRawrRinth" )     { SetGotoState( "rawrrinthstate" ); } //{ SetGotoState( "rawrrinthmenustate" ); }
        else if ( clickedButton == "btnTest" )
        {
            SetGotoState( "gamestate" );
        }
        else if ( clickedButton == "btnOptions" )
        {
        }
        else if ( clickedButton == "btnHelp" )
        {
        }
        else if ( clickedButton == "btnQuit" )
        {
            chalo::Application::ReadyToQuit();
        }
    }

//
//    std::string clickedButton = chalo::MenuManager::GetClickedButton();
//
//
//    for ( int i = 0; i < 15; i++ )
//    {
//        if ( chalo::InputManager::IsJoystickButtonPressed( 0, i ) )
//        {
//            std::cout << i << std::endl;
//        }
//    }

}

void StartupState::Draw( sf::RenderWindow& window )
{
    VamooseState::Draw( window );
}




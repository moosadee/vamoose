#include "ShoppingMallState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"


#include "../Packages/ShoppingMall/rjm_bodyPartClass.h"
#include "../Packages/ShoppingMall/rjm_buttonClass.h"
#include "../Packages/ShoppingMall/rjm_gridClass.h"
#include "../Packages/ShoppingMall/rjm_mapClass.h"
#include "../Packages/ShoppingMall/rjm_playerClass.h"
#include "../Packages/ShoppingMall/rjm_npcClass.h"
#include "../Packages/ShoppingMall/common.h"

namespace ShoppingMall
{

ShoppingMallState::ShoppingMallState()
    : VamooseState()
{
}

void ShoppingMallState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "ShoppingMallState::Init", "function-trace" );
    VamooseState::Init( name );
}

void ShoppingMallState::Setup()
{
    chalo::Logger::Out( "", "ShoppingMallState::Setup", "function-trace" );
    VamooseState::Setup();

    // Which state comes before?
    m_backState = "startupstate";

    chalo::TextureManager::Add( "shopping_clerks",          "Packages/ShoppingMall/content/shopping_clerks.png" );
    chalo::TextureManager::Add( "shopping_sprite_body",     "Packages/ShoppingMall/content/shopping_sprite_body.png" );
    chalo::TextureManager::Add( "shopping_sprite_clothes",  "Packages/ShoppingMall/content/shopping_sprite_clothes.png" );
    chalo::TextureManager::Add( "shopping_sprite_hair",     "Packages/ShoppingMall/content/shopping_sprite_hair.png" );
    chalo::TextureManager::Add( "shopping_sprite_makeup",   "Packages/ShoppingMall/content/shopping_sprite_makeup.png" );
    chalo::TextureManager::Add( "shopping_sprite_shoes",    "Packages/ShoppingMall/content/shopping_sprite_shoes.png" );
    chalo::TextureManager::Add( "shoppingTileset",          "Packages/ShoppingMall/content/shoppingTileset.png" );

    init_clerks(clerk);

    map[0].load_map("mall_bottom");
    map[1].load_map("mall_top");
    map[2].load_map("clothes");
    map[3].load_map("hair");
    map[4].load_map("makeup");
    map[5].load_map("shoes");
    two_player = true;
    init_buttons(button);
    game_anim = 0.0;
    counter = 0.0;

    totalPlayers = 4;
    for ( int i = 0; i < totalPlayers; i++ )
    {
        player[i].setup( i );
    }

    tileset.setTexture( chalo::TextureManager::Get( "shoppingTileset" ) );
    clerkimg.setTexture( chalo::TextureManager::Get( "shopping_clerks" ) );

    // Which state comes before?
    m_backState = "startupstate";
}

void ShoppingMallState::Cleanup()
{
    chalo::Logger::Out( "", "ShoppingMallState::Cleanup", "function-trace" );
    VamooseState::Cleanup();
}

void ShoppingMallState::Update()
{
    VamooseState::Update();

    int playerIndex;
    bool playerMoved;

    // PLAYER 1

    playerMoved = false;
    for ( playerIndex = 0; playerIndex < totalPlayers; playerIndex++ )
    {
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_WEST ) ) )
        {
            move_player( &player[playerIndex], map[map_index], "left", counter, clerk, map_index );
            playerMoved = true;
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_EAST ) ) )
        {
            move_player( &player[playerIndex], map[map_index], "right", counter, clerk, map_index );
            playerMoved = true;
        }
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_NORTH ) ) )
        {
            move_player( &player[playerIndex], map[map_index], "up", counter, clerk, map_index );
            playerMoved = true;
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( playerIndex, chalo::INPUT_SOUTH ) ) )
        {
            move_player( &player[playerIndex], map[map_index], "down", counter, clerk, map_index );
            playerMoved = true;
        }

        if ( playerMoved )
        {
            player[playerIndex].increment_frame();
            int move = check_movement( player, map[map_index], two_player );
            check_clerk_collision( player, clerk, map_index );
            if ( move != -1 )
            {
                if ( playerIndex == 0 )
                {
                    int old = map_index;
                    go_to_map( map, player, move, &map_index );
                    if ( (old == 0 && map_index == 1) || (old == 1 && map_index == 0 ) ) { ; }
                }
                else if ( map_index == 0 )
                {
                    //                play_midi(song[1], true);
                }
                else
                {
                    //                play_midi(song[map_index], true);
                }
            }
        }

    }

    counter += 0.1;
    game_anim += 0.1;
    if ( game_anim > 2.0 )
    {
        game_anim = 0.0;
    }
    if ( counter > 5.0 )
    {
        counter = 0.0;
    }
    for (int i=0; i<6; i++)
    {
        if ( clerk[i].talking )
        {
            clerk[i].increment_counter();
        }
    }

    VamooseState::BackButtonHandler();
}

void ShoppingMallState::Draw( sf::RenderWindow& window )
{
    VamooseState::Draw( window );

    map[map_index].draw(window, game_anim);

    for (int i=0; i<6; i++)
    {
        if ( clerk[i].map == map_index )
        {
            clerk[i].draw(window, clerkimg);
        }
    }

    for ( int i = 0; i < totalPlayers; i++ )
    {
        player[i].draw( window );
    }

    //textprintf(buffer, font, 0, 5, makecol(255, 255, 255), "(%i, %i)", player[0].x, player[0].y);
    map[map_index].draw_top(window, game_anim);
}

void ShoppingMallState::check_click(buttonClass button, int mouse_x, int mouse_y, int index, bool *done, bool *two_player, bool *menu)
{
    int left1 = mouse_x, left2, right1 = mouse_x + 40, right2, top1 = mouse_y, top2, bottom1 = mouse_y + 40, bottom2;
    left2 = button.x;
    right2 = left2 + button.w;
    top2 = button.y;
    bottom2 = top2 + button.h;
    if (    (bottom2 > top1) && (top2 < bottom1)    &&
            (right2 > left1) && (left2 < right1))
    {
        //clicked button
        if ( index == 0 )
        {
            (*menu) = false;
            (*two_player) = false;
//            play_midi(mall_song, true);
        }
        else if ( index == 1 )
        {
            (*two_player) = true;
            (*menu) = false;
//            play_midi(mall_song, true);
        }
        else if ( index == 2 )
        {
            (*done) = true;
        }
    }
}

void ShoppingMallState::init_buttons(buttonClass button[btn_amt])
{
    button[0].x = 315;
    button[0].y = 400;
    button[0].w = 100;
    button[0].h = 100;
    button[0].grid_x = 50;
    button[0].grid_y = 600;
    button[0].go_to = "oneplayer";

    button[1].x = 435;
    button[1].y = 400;
    button[1].w = 100;
    button[1].h = 100;
    button[1].grid_x = 150;
    button[1].grid_y = 600;
    button[1].go_to = "twoplayer";

    button[2].x = 400;
    button[2].y = 525;
    button[2].w = 50;
    button[2].h = 50;
    button[2].grid_x = 0;
    button[2].grid_y = 600;
    button[2].go_to = "exit";
}


int ShoppingMallState::check_movement(rjm_playerClass player[4], rjm_mapClass map, bool two_player)
{
    //This is for "warp" collision -- the doorways, basically, to get to another map
    // bottom2 > top1, top2 < bottom1
    // right2 > left1, left2 < right1
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            if (    ( player[j].x + player[j].w     >       map.warp_x1[i] ) &&
                    ( player[j].x                   <       map.warp_x2[i] ) &&
                    ( player[j].y + player[j].h     >       map.warp_y1[i] ) &&
                    ( player[j].y + player[j].h/2   <       map.warp_y2[i] ) )
            {
                return i;
            }
        }
    }
    return -1;
}

void ShoppingMallState::check_clerk_collision( rjm_playerClass player[4], rjm_npcClass clerk[6], int map_index )
{
    // right2 > left1, left2 < right1
    // bottom2 > top1, top2 < bottom1
    for (int i=0; i<6; i++)
    {
        if ( clerk[i].map == map_index )
        {
            for (int j=0; j<2; j++)
            {
                if (    ( player[j].x + player[j].w     >       clerk[i].talkx ) &&
                        ( player[j].x                   <       clerk[i].talkx + 32 ) &&
                        ( player[j].y + player[j].h     >       clerk[i].talky ) &&
                        ( player[j].y + player[j].h/2   <       clerk[i].talky + 32 ) )
                {
                    //collision
                    if ( clerk[i].talking == false )
                    {
                        int random = (int)rand()%2;
//                        play_sample( clerk[i].speak[random], 255, 128, 1000, false );
                        clerk[i].talking = true;
//                        play_sample( clerk[i].speak[random], 255, 128, 1000, false );
                        clerk[i].talking = true;
                    }
                }
            }
        }
    }
}


void ShoppingMallState::init_clerks( rjm_npcClass clerk[6] )
{
    for (int i=0; i<6; i++)
    {
        clerk[i].codex = i*48;
        clerk[i].w = 48;
        clerk[i].h = 96;
    }
    clerk[0].map = 2;           //clothing store
    clerk[0].x = 186;
    clerk[0].y = 122;
    clerk[0].talkx = clerk[0].x;
    clerk[0].talky = clerk[0].y + 96;
//    clerk[0].speak[0] = load_sample("shopping_c1_a.wav");
//    clerk[0].speak[1] = load_sample("shopping_c1_b.wav");

    clerk[1].map = 3;           //hair salon
    clerk[1].x = 348;
    clerk[1].y = 102;
    clerk[1].talkx = clerk[1].x;
    clerk[1].talky = clerk[1].y + 96;
//    clerk[1].speak[0] = load_sample("shopping_c2_a.wav");
//    clerk[1].speak[1] = load_sample("shopping_c2_b.wav");

    clerk[2].map = 3;           //hair salon
    clerk[2].x = 566;
    clerk[2].y = 102;
    clerk[2].talkx = clerk[2].x;
    clerk[2].talky = clerk[2].y + 96;
//    clerk[2].speak[0] = load_sample("shopping_c4_a.wav");
//    clerk[2].speak[1] = load_sample("shopping_c4_b.wav");

    clerk[3].map = 4;           //makeup store
    clerk[3].x = 122;
    clerk[3].y = 196;
    clerk[3].talkx = clerk[3].x;
    clerk[3].talky = clerk[3].y + 96;
//    clerk[3].speak[0] = load_sample("shopping_c5_a.wav");
//    clerk[3].speak[1] = load_sample("shopping_c5_b.wav");

    clerk[4].map = 4;           //makeup store
    clerk[4].x = 666;
    clerk[4].y = 78;
    clerk[4].talkx = clerk[4].x;
    clerk[4].talky = clerk[4].y + 100;
//    clerk[4].speak[0] = load_sample("shopping_c3_a.wav");
//    clerk[4].speak[1] = load_sample("shopping_c3_b.wav");

    clerk[5].map = 5;           //shoe store
    clerk[5].x = 212;
    clerk[5].y = 90;
    clerk[5].talkx = 212;
    clerk[5].talky = 90 + clerk[5].h;
//    clerk[5].speak[0] = load_sample("shopping_c6_a.wav");
//    clerk[5].speak[1] = load_sample("shopping_c6_b.wav");
}


void ShoppingMallState::move_player( rjm_playerClass *player, rjm_mapClass map, string direction, float temp_count, rjm_npcClass clerk[6], int map_index )
{
    bool collide = false;
    bool hair = false;
    bool makeup = false;
    bool shoes = false;
    bool clothes = false;
    int x = player->x + 8;
    int y = player->y + player->h - 32;
    int code;
    int codeB;
    int codeC;
    if ( direction == "up" )
    {
        y -= player->speed;
    }
    else if ( direction == "down" )
    {
        y += player->speed+0.5;
    }
    else if ( direction == "left" )
    {
        x -= player->speed;
    }
    else if ( direction == "right" )
    {
        x += player->speed+0.5;
    }
    for (int i=0; i<8; i++)
    {
        if ( i==0 )
        {
            code = map.bottom_layer[x/32][y/32].codex;
        }
        else if ( i==1 )
        {
            code = map.bottom_layer[x/32+1][y/32].codex;
        }
        else if ( i==2 )
        {
            code = map.bottom_layer[x/32][y/32+1].codex;
        }
        else if ( i==3 )
        {
            code = map.bottom_layer[x/32+1][y/32+1].codex;
        }
        else if ( i==4 )
        {
            code = map.top_layer[x/32][y/32].codex;
        }
        else if ( i==5 )
        {
            code = map.top_layer[x/32+1][y/32].codex;
        }
        else if ( i==6 )
        {
            code = map.top_layer[x/32][y/32+1].codex;
        }
        else if ( i==7 )
        {
            code = map.top_layer[x/32+1][y/32+1].codex;
        }
        if ( i < 4 )
        {
            if ( (code != 416 ) && (code != 448) && (code != 224) && (code != 320) && (code != 128) )
            {
                collide = true;
            }
        }
        else
        {
            if ( (code != 0) && (code != 416 ) && (code != 448) && (code != 224) && (code != 320) && (code != 128) )
            {
                collide = true;
                if ( code == 85*32 || code == 86*32 )
                {
                    clothes = true;
                }
                else if ( code == 93*32 || code == 94*32 )
                {
                    makeup = true;
                }
                else if ( code == 95*32 || code == 96*32 || code == 97*32 ||
                          code == 98*32 || code == 99*32 || code == 100*32 )
                {
                    shoes = true;
                }
                else if ( code == 108*32 || code == 107*32 )
                {
                    hair = true;
                }
            }
        }
    }
    x = player->x;
    y = player->y;
    if ( direction == "up" )
    {
        y -= player->speed;
    }
    else if ( direction == "down" )
    {
        y += player->speed;
    }
    else if ( direction == "left" )
    {
        x -= player->speed;
    }
    else if ( direction == "right" )
    {
        x += player->speed;
    }
    //check for collision with a clerk
    for (int j=0; j<6; j++)
    {
        if ( clerk[j].map == map_index )
        {
            if (    ( x + player->w     >       clerk[j].x ) &&
                    ( x                 <       clerk[j].x + clerk[j].w ) &&
                    ( y + player->h     >       clerk[j].y ) &&
                    ( y + player->h/2   <       clerk[j].y + clerk[j].h ) )
            {
                //collision

                collide = true;
            }
        }
    }
    if ( collide == false )
    {
        if ( direction == "up" )
        {
            player->move("up");
        }
        else if ( direction == "down" )
        {
            player->move("down");
        }
        else if ( direction == "left" )
        {
            player->move("left");
        }
        else if ( direction == "right" )
        {
            player->move("right");
        }
        player->force_update();
    }
    else
    {
        if ( clothes && temp_count == 0.0 )
        {
            player->swap('c');
//            play_sample(sfx, 255, 128, 1000, false);
            player->direction="down";
        }
        else if ( hair && temp_count == 0.0 )
        {
            player->swap('h');
//            play_sample(sfx, 255, 128, 1000, false);
            player->direction="down";
        }
        else if ( shoes && temp_count == 0.0)
        {
            player->swap('s');
//            play_sample(sfx, 255, 128, 1000, false);
            player->direction="down";
        }
        else if ( makeup && temp_count == 0.0 )
        {
            player->swap('m');
//            play_sample(sfx, 255, 128, 1000, false);
            player->direction="down";
        }
    }
}

void ShoppingMallState::go_to_map( rjm_mapClass map[6], rjm_playerClass player[4], int warp_index, int *map_index )
{
    int oldmap = *map_index;
    int newmap = map[oldmap].go_to[warp_index];
    if ( newmap == -1 )
    {
        return;
    }
    *map_index = newmap;
    player[0].x = map[newmap].enter_x[oldmap];
    player[0].y = map[newmap].enter_y[oldmap];

    player[1].x = map[newmap].enter_x[oldmap] + 32;
    player[1].y = map[newmap].enter_y[oldmap];

    player[2].x = map[newmap].enter_x[oldmap] + 16;
    player[2].y = map[newmap].enter_y[oldmap];

    player[3].x = map[newmap].enter_x[oldmap] + 8;
    player[3].y = map[newmap].enter_y[oldmap];

    player[0].force_update();
    player[1].force_update();
    player[2].force_update();
    player[3].force_update();
}

}

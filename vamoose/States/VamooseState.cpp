#include "VamooseState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"

VamooseState::VamooseState()
{
}

void VamooseState::Init( const std::string& name, bool useCursors )
{
    chalo::Logger::Out( "Parameters - name: " + name, "VamooseState::Init", "function-trace" );
    IState::Init( name );
    m_useCursors = useCursors;
}

void VamooseState::Setup()
{
    chalo::Logger::Out( "", "VamooseState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "adminFlags", "Content/Graphics/UI/admin-flags.png" );

    if ( m_useCursors )
    {
        chalo::TextureManager::Add( "cursor1",  "Content/Graphics/UI/cursor1.png" );
        chalo::TextureManager::Add( "cursor2",  "Content/Graphics/UI/cursor2.png" );
        chalo::TextureManager::Add( "cursor3",  "Content/Graphics/UI/cursor3.png" );
        chalo::TextureManager::Add( "cursor4",  "Content/Graphics/UI/cursor4.png" );
        CursorSetup();
    }

    // Visual flag
    m_backTimer = 0;
    m_adminBlep.setTexture( chalo::TextureManager::Get( "adminFlags" ) );

    m_logTimer = 0;
}

void VamooseState::Cleanup()
{
    chalo::Logger::Out( "", "VamooseState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    chalo::TextureManager::Clear();
    m_cursors.clear();
}

void VamooseState::Update()
{
    m_logTimer++;
    if ( m_logTimer > 500 )
    {
        chalo::Logger::Out( "", "VamooseState::Update", "function-trace" );
        m_logTimer = 0;
    }

    if ( m_useCursors )
    {
        CursorUpdate();
    }

    chalo::InputManager::Update();
    chalo::MenuManager::Update();
}

void VamooseState::Draw( sf::RenderWindow& window )
{
    chalo::DrawManager::Draw( window );
    chalo::DrawManager::AddMenu();

    if ( m_useCursors )
    {
        CursorDraw();
    }
}

void VamooseState::BackButtonHandler()
{
    // Check for back command
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION6 ) ) )
    {
        chalo::Logger::Out( "Holding back... " + chalo::StringUtility::IntegerToString( m_backTimer ), "VamooseState::BackButtonHandler", "function-trace" );
        chalo::DrawManager::AddSprite( m_adminBlep );
        m_backTimer++;
    }
    else
    {
        m_backTimer = 0;
    }

    if ( m_backTimer >= 100 )
    {
        SetGotoState( m_backState );
    }
}

void VamooseState::CursorSetup()
{
    int centerX = chalo::Application::GetScreenWidth() / 2 - 31;
    int centerY = chalo::Application::GetScreenHeight() / 2 - 31;
    sf::IntRect frame = sf::IntRect( 0, 0, 62, 62 );

    chalo::Character cursor1;
    cursor1.Setup();
    cursor1.SetTexture( chalo::TextureManager::Get( "cursor1" ), frame );
    cursor1.SetActiveState( chalo::ACTIVE );
    cursor1.SetPosition( centerX, centerY );
    cursor1.SetAnimationInformation( 1, 0.1 );
    m_cursors.push_back( cursor1 );

    chalo::Character cursor2;
    cursor2.Setup();
    cursor2.SetTexture( chalo::TextureManager::Get( "cursor2" ), frame );
    cursor2.SetActiveState( chalo::INVISIBLE );
    cursor2.SetPosition( centerX, centerY );
    m_cursors.push_back( cursor2 );

    chalo::Character cursor3;
    cursor3.Setup();
    cursor3.SetTexture( chalo::TextureManager::Get( "cursor3" ), frame );
    cursor3.SetActiveState( chalo::INVISIBLE );
    cursor3.SetPosition( centerX, centerY );
    m_cursors.push_back( cursor3 );

    chalo::Character cursor4;
    cursor4.Setup();
    cursor4.SetTexture( chalo::TextureManager::Get( "cursor4" ), frame );
    cursor4.SetActiveState( chalo::INVISIBLE );
    cursor4.SetPosition( centerX, centerY );
    m_cursors.push_back( cursor4 );
}

void VamooseState::CursorUpdate()
{

    // Check inputs
    for ( int i = 0; i < 4; i++ )
    {
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_WEST ) ) )
        {
            m_cursors[i].Move( chalo::WEST );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_EAST ) ) )
        {
            m_cursors[i].Move( chalo::EAST );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_NORTH ) ) )
        {
            m_cursors[i].Move( chalo::NORTH );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_SOUTH ) ) )
        {
            m_cursors[i].Move( chalo::SOUTH );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
    }
}

void VamooseState::CursorDraw()
{
    for ( auto& cursor : m_cursors )
    {
        if ( cursor.GetActiveState() == chalo::ACTIVE )
        {
            cursor.ForceSpriteUpdate();
            chalo::DrawManager::AddSprite( cursor.GetSprite() );
        }
    }
}

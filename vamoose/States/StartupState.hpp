#ifndef _STARTUPSTATE
#define _STARTUPSTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"

#include "VamooseState.hpp"

#include <vector>

class StartupState : public VamooseState
{
public:
    StartupState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );
};

#endif

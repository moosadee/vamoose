#include "QuixAttaxDeuxState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Application/Application.hpp"

QuixAttaxDeuxState::QuixAttaxDeuxState()
    : VamooseState(), BULLET_COOLDOWN_MAX( 20 )
{
}

void QuixAttaxDeuxState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "QuixAttaxDeuxState::Init", "function-trace" );
    VamooseState::Init( name );
}

void QuixAttaxDeuxState::Setup()
{
    chalo::Logger::Out( "", "QuixAttaxDeuxState::Setup", "function-trace" );
    VamooseState::Setup();

    chalo::TextureManager::Add( "character", "Content/Graphics/Characters/spritesheet-ayda-small.png" );

//    m_player.Setup();
//    m_player.SetTexture( chalo::TextureManager::Get( "character" ), 0, 0, 40, 40 );
//    m_player.SetPosition( 200, 200 );
//    m_player.SetAnimationInformation( 4, 0.1 );
//    chalo::DrawManager::AddSprite( m_player.GetSprite() );

//    BITMAP *buffer = create_bitmap(800,600);

    chalo::TextureManager::Add( "character1", "Packages/QuixAttaxDeux/content/quiximgPlayerSet1.png" );
    chalo::TextureManager::Add( "character2", "Packages/QuixAttaxDeux/content/quiximgPlayerSet2.png" );
    chalo::TextureManager::Add( "character3", "Packages/QuixAttaxDeux/content/quiximgPlayerSet3.png" );
    chalo::TextureManager::Add( "character4", "Packages/QuixAttaxDeux/content/quiximgPlayerSet4.png" );
    chalo::TextureManager::Add( "bmpBackground", "Packages/QuixAttaxDeux/content/quiximgBackgroundGrass.png" );
    chalo::TextureManager::Add( "bmpPlayerFilmstrip", "Packages/QuixAttaxDeux/content/quiximgPlayerSet.png" );
    chalo::TextureManager::Add( "bmpLaserFilmstrip", "Packages/QuixAttaxDeux/content/quiximgLasers.png" );

    for ( int i = 0; i < 4; i++ )
    {
        std::string index = chalo::StringUtility::IntegerToString( i );
        m_isActive[i]           = chalo::Messager::GetInt( "IsPlayer"    + index + "Active" );
        m_selectedCharacters[i] = chalo::Messager::GetInt( "Player"      + index + "SelectedCharacter" );

        if ( !m_isActive[i] )
        {
            m_players[i].SetActiveState( chalo::INVISIBLE );
        }
    }

    m_barrierOption = chalo::Messager::GetInt( "BarrierOption" );
    m_fragOption = chalo::Messager::GetInt( "FragOption" );

    int screenWidth = chalo::Application::GetScreenWidth();
    int screenHeight = chalo::Application::GetScreenHeight();
    int spriteWH = 35;

    m_players[0].Setup( spriteWH,  spriteWH,  "player0" );
    m_players[0].SetTexture( chalo::TextureManager::Get( "character1" ), 0, 0, spriteWH, spriteWH );
    m_players[0].SetAnimationInformation( 3, 0.1 );

    m_players[1].Setup( spriteWH,  screenHeight - spriteWH * 2, "player1" );
    m_players[1].SetTexture( chalo::TextureManager::Get( "character2" ), 0, 0, spriteWH, spriteWH );
    m_players[1].SetAnimationInformation( 3, 0.1 );

    m_players[2].Setup( screenWidth - spriteWH * 2, screenHeight - spriteWH * 2, "player2" );
    m_players[2].SetTexture( chalo::TextureManager::Get( "character3" ), 0, 0, spriteWH, spriteWH );
    m_players[2].SetAnimationInformation( 3, 0.1 );

    m_players[3].Setup( screenWidth - spriteWH * 2, spriteWH,  "player3" );
    m_players[3].SetTexture( chalo::TextureManager::Get( "character4" ), 0, 0, spriteWH, spriteWH );
    m_players[3].SetAnimationInformation( 3, 0.1 );

    // Set up grass
    for ( int y = 0; y < screenHeight/32 + 1; y++ )
    {
        for ( int x = 0; x < screenWidth/32; x++ )
        {
            chalo::Map::Tile tile;
            tile.SetTexture( chalo::TextureManager::Get( "bmpBackground" ), 0, 0, 32, 32 );
            tile.SetPosition( x * 32, y * 32 );
            m_mapTiles.push_back( tile );
        }
    }

    // Which state comes before?
    //m_backState = "quixattaxdeuxmenustate";
    m_backState = "startupstate";
}

void QuixAttaxDeuxState::Cleanup()
{
    chalo::Logger::Out( "", "QuixAttaxDeuxState::Cleanup", "function-trace" );
    VamooseState::Cleanup();
}

void QuixAttaxDeuxState::Update()
{
    VamooseState::Update();

    int spriteWH = m_players[0].GetDimensions().x;
    int maxX = chalo::Application::GetScreenWidth() - spriteWH;
    int maxY = chalo::Application::GetScreenHeight() - spriteWH;

    // Ground
    for ( auto& tile : m_mapTiles )
    {
        chalo::DrawManager::AddSprite( tile.GetSprite() );
    }

    // Player inputs
    for ( int i = 0; i < 4; i++ )
    {
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_WEST ) ) )
        {
            m_players[i].Move( chalo::WEST, 0, 0, maxX, maxY );
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_EAST ) ) )
        {
            m_players[i].Move( chalo::EAST, 0, 0, maxX, maxY );
        }

        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_NORTH ) ) )
        {
            m_players[i].Move( chalo::NORTH, 0, 0, maxX, maxY );
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_SOUTH ) ) )
        {
            m_players[i].Move( chalo::SOUTH, 0, 0, maxX, maxY );
        }

        // Shoot
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_ACTION1 ) ) )
        {
            Shoot( i );
        }

        m_players[i].ForceSpriteUpdate();

        if ( m_players[i].GetActiveState() != chalo::INVISIBLE )
        {
            chalo::DrawManager::AddSprite( m_players[i].GetSprite() );
        }
    }

    // Update bullets
    ClearoutInactiveBullets();
    int bulletWH = 20;
    maxX = chalo::Application::GetScreenWidth() - bulletWH;
    maxY = chalo::Application::GetScreenHeight() - bulletWH;
    for ( auto& bullet : m_bullets )
    {
        bullet.Update();
        if ( bullet.IsOffscreen( 0, 0, maxX, maxY ) )
        {
            bullet.SetActiveState( chalo::WAITING_DELETION );
        }

        if ( bullet.GetActiveState() == chalo::ACTIVE )
        {
            bullet.ForceSpriteUpdate();
            chalo::DrawManager::AddSprite( bullet.GetSprite() );
        }
    }

    // Bullet cooldown
    for ( auto& cooldown : m_bulletCooldown )
    {
        if ( cooldown > 0 )
        {
            cooldown--;
        }
    }

    VamooseState::BackButtonHandler();
}

void QuixAttaxDeuxState::Shoot( int playerIndex )
{
    if ( m_bulletCooldown[playerIndex] > 0 )
    {
        return;
    }

    chalo::Direction dir = m_players[playerIndex].GetDirection();
    sf::Vector2f startingPosition = m_players[playerIndex].GetCenterPosition();
    int playerType = m_selectedCharacters[playerIndex];
    int bulletWH = 20;

    int tilesetX = playerIndex * bulletWH;
    int tilesetY = (playerType-1) * bulletWH;

    chalo::Projectile bullet;
    bullet.SetTexture( chalo::TextureManager::Get( "bmpLaserFilmstrip" ), tilesetX, tilesetY, bulletWH, bulletWH );
    bullet.SetDirection( dir );
    bullet.SetPosition( startingPosition );
    bullet.SetSpeed( 10 );
    m_bullets.push_back( bullet );

    m_bulletCooldown[playerIndex] = BULLET_COOLDOWN_MAX;
}

void QuixAttaxDeuxState::ClearoutInactiveBullets()
{
    std::list<int> deleteIndices;
    for ( unsigned int i = 0; i < m_bullets.size(); i++ )
    {
        if ( m_bullets[i].GetActiveState() != chalo::ACTIVE )
        {
            // Larger indices go at the front
            deleteIndices.push_front( i );
        }
    }

    // Delete back-to-front index
    for ( auto& index : deleteIndices )
    {
        m_bullets.erase( m_bullets.begin() + index );
    }
}

void QuixAttaxDeuxState::Draw( sf::RenderWindow& window )
{
    VamooseState::Draw( window );
}


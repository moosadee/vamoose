#include "ChaloEngineProgram.hpp"

#include "../States/StartupState.hpp"
#include "../States/GameState.hpp"
#include "../States/QuixAttaxDeuxState.hpp"
#include "../States/QuixAttaxDeuxMenuState.hpp"
#include "../States/BootlegBashMenuState.hpp"
#include "../States/ShoppingMallMenuState.hpp"
#include "../States/ShoppingMallState.hpp"
#include "../States/RawrRinthMenuState.hpp"
#include "../States/RawrRinthState.hpp"

#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"

ChaloEngineProgram::ChaloEngineProgram( bool fullscreen )
{
    Setup( fullscreen );
}

ChaloEngineProgram::~ChaloEngineProgram()
{
    Teardown();
}

void ChaloEngineProgram::Setup( bool fullscreen )
{
    std::string full = ( fullscreen ) ? "1" : "0";

    std::map<std::string, std::string> options = {
        std::pair<std::string, std::string>( "CONFIG_NAME", "config.chaloconfig" ),
        std::pair<std::string, std::string>( "TITLEBAR_TEXT", "VAMOOSE" ),
        std::pair<std::string, std::string>( "WINDOW_WIDTH", "1280" ),
        std::pair<std::string, std::string>( "WINDOW_HEIGHT", "720" ),
        std::pair<std::string, std::string>( "MENU_PATH", "Contents/Menus/" ),
        std::pair<std::string, std::string>( "FULLSCREEN", full )
    };

    chalo::ChaloProgram::Setup( options );

    // Set up menu manager
    chalo::MenuManager::Setup( "Content/Menus/" );

    // Set up global assets
    chalo::FontManager::Add( "main", "Content/Fonts/mononoki-Bold.ttf" );
    chalo::FontManager::Add( "videogame", "Content/Fonts/PressStart2P.ttf" );

    // Set up draw managr
    chalo::DrawManager::Setup();

    chalo::InputManager::Setup();
    SetupKeybindings();

    // Set up states
    chalo::IState* startupState = new StartupState;
    chalo::IState* gameState    = new GameState;
    chalo::IState* quixattaxdeux_menuState    = new QuixAttaxDeuxMenuState;
    chalo::IState* quixattaxdeux_gameState    = new QuixAttaxDeuxState;
    chalo::IState* bootlegbash_menuState      = new BootlegBashMenuState;
    chalo::IState* shoppingmall_menuState     = new ShoppingMall::ShoppingMallMenuState;
    chalo::IState* shoppingmall_gameState     = new ShoppingMall::ShoppingMallState;
    chalo::IState* rawrrinth_menuState        = new RawrRinth::RawrRinthMenuState;
    chalo::IState* rawrrinth_gameState        = new RawrRinth::RawrRinthGameState;

    m_stateManager.InitManager();
    m_stateManager.AddState( "startupstate", startupState );
    m_stateManager.AddState( "gamestate", gameState );
    m_stateManager.AddState( "quixattaxdeuxmenustate", quixattaxdeux_menuState );
    m_stateManager.AddState( "quixattaxdeuxstate", quixattaxdeux_gameState );
    m_stateManager.AddState( "bootlegbashmenustate", bootlegbash_menuState );
    m_stateManager.AddState( "shoppingmallmenustate", shoppingmall_menuState );
    m_stateManager.AddState( "shoppingmallstate", shoppingmall_gameState );
    m_stateManager.AddState( "rawrrinthmenustate", rawrrinth_menuState );
    m_stateManager.AddState( "rawrrinthstate", rawrrinth_gameState );

    /* testing quix attax */
    // temp
    for ( int i = 0; i < 4; i++ )
    {
        std::string index = chalo::StringUtility::IntegerToString( i );
        chalo::Messager::Set( "IsPlayer"    + index + "Active", true );
        chalo::Messager::Set( "Player"      + index + "SelectedCharacter", 1 );
    }

    chalo::Messager::Set( "BarrierOption", 0 );
    chalo::Messager::Set( "FragOption", 0 );
    // temp

    m_stateManager.ChangeState( "startupstate" );
}

void ChaloEngineProgram::SetupKeybindings()
{
    chalo::InputManager::SetKeybindings( {
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_NORTH ),  { sf::Keyboard::W }, {}, { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::Y, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_SOUTH ),  { sf::Keyboard::S }, {}, { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::Y, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_WEST ),   { sf::Keyboard::A }, {}, { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::X, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_EAST ),   { sf::Keyboard::D }, {}, { chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::X, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ),{ sf::Keyboard::Q }, { chalo::JoystickButtonBinding( 0, 0 ), chalo::JoystickButtonBinding( 0, 2 ), chalo::JoystickButtonBinding( 0, 9 ) }, {} ),
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION2 ),{ sf::Keyboard::E }, { chalo::JoystickButtonBinding( 0, 1 ), chalo::JoystickButtonBinding( 0, 3 ) }, {} ),
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION6 ),{ sf::Keyboard::BackSpace }, { chalo::JoystickButtonBinding( 0, 8 ) }, {} ), // Select button

        chalo::InputBinding( chalo::PlayerInputAction( 1, chalo::INPUT_NORTH ),  { sf::Keyboard::I }, {}, { chalo::JoystickAxisBinding( 1, sf::Joystick::Axis::Y, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 1, chalo::INPUT_SOUTH ),  { sf::Keyboard::K }, {}, { chalo::JoystickAxisBinding( 1, sf::Joystick::Axis::Y, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 1, chalo::INPUT_WEST ),   { sf::Keyboard::J }, {}, { chalo::JoystickAxisBinding( 1, sf::Joystick::Axis::X, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 1, chalo::INPUT_EAST ),   { sf::Keyboard::L }, {}, { chalo::JoystickAxisBinding( 1, sf::Joystick::Axis::X, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 1, chalo::INPUT_ACTION1 ),{ sf::Keyboard::U }, { chalo::JoystickButtonBinding( 1, 0 ), chalo::JoystickButtonBinding( 1, 2 ), chalo::JoystickButtonBinding( 1, 9 ) }, {} ),
        chalo::InputBinding( chalo::PlayerInputAction( 1, chalo::INPUT_ACTION2 ),{ sf::Keyboard::O }, { chalo::JoystickButtonBinding( 1, 1 ), chalo::JoystickButtonBinding( 1, 3 ) }, {} ),

        chalo::InputBinding( chalo::PlayerInputAction( 2, chalo::INPUT_NORTH ),  { sf::Keyboard::Up },       {}, { chalo::JoystickAxisBinding( 2, sf::Joystick::Axis::Y, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 2, chalo::INPUT_SOUTH ),  { sf::Keyboard::Down },     {}, { chalo::JoystickAxisBinding( 2, sf::Joystick::Axis::Y, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 2, chalo::INPUT_WEST ),   { sf::Keyboard::Left },     {}, { chalo::JoystickAxisBinding( 2, sf::Joystick::Axis::X, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 2, chalo::INPUT_EAST ),   { sf::Keyboard::Right },    {}, { chalo::JoystickAxisBinding( 2, sf::Joystick::Axis::X, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 2, chalo::INPUT_ACTION1 ),{ sf::Keyboard::RAlt },     { chalo::JoystickButtonBinding( 2, 0 ), chalo::JoystickButtonBinding( 2, 2 ), chalo::JoystickButtonBinding( 2, 9 ) }, {} ),
        chalo::InputBinding( chalo::PlayerInputAction( 2, chalo::INPUT_ACTION2 ),{ sf::Keyboard::RControl }, { chalo::JoystickButtonBinding( 2, 1 ), chalo::JoystickButtonBinding( 2, 3 ) }, {} ),

        chalo::InputBinding( chalo::PlayerInputAction( 3, chalo::INPUT_NORTH ),  { sf::Keyboard::Numpad8 }, {}, { chalo::JoystickAxisBinding( 3, sf::Joystick::Axis::Y, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 3, chalo::INPUT_SOUTH ),  { sf::Keyboard::Numpad2 }, {}, { chalo::JoystickAxisBinding( 3, sf::Joystick::Axis::Y, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 3, chalo::INPUT_WEST ),   { sf::Keyboard::Numpad4 }, {}, { chalo::JoystickAxisBinding( 3, sf::Joystick::Axis::X, -100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 3, chalo::INPUT_EAST ),   { sf::Keyboard::Numpad6 }, {}, { chalo::JoystickAxisBinding( 3, sf::Joystick::Axis::X, 100 ) } ),
        chalo::InputBinding( chalo::PlayerInputAction( 3, chalo::INPUT_ACTION1 ),{ sf::Keyboard::Numpad7 }, { chalo::JoystickButtonBinding( 3, 0 ), chalo::JoystickButtonBinding( 3, 2 ), chalo::JoystickButtonBinding( 3, 9 ) }, {} ),
        chalo::InputBinding( chalo::PlayerInputAction( 3, chalo::INPUT_ACTION2 ),{ sf::Keyboard::Numpad9 }, { chalo::JoystickButtonBinding( 3, 1 ), chalo::JoystickButtonBinding( 3, 3 ) }, {} ),
    } );
}

void ChaloEngineProgram::Run()
{
    while ( chalo::Application::IsRunning() )
    {
        chalo::Application::BeginDrawing();
        chalo::Application::Update();
        m_stateManager.UpdateState();

        if ( m_stateManager.GetGotoState() != "" )
        {
            m_stateManager.ChangeState( m_stateManager.GetGotoState() );
        }

        // Drawing
        m_stateManager.DrawState( chalo::Application::GetWindow() );
        chalo::Application::EndDrawing();
    }
}

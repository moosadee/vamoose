Rachel J. Morris [Moosader.com](http://www.moosader.com/)

## ABOUT
Rawr Rinth was a small action-rpg I made for my Software Development
class at UMKC.

## MISSING ASSETS
I deleted assets that I did not own.
Some of my old projects used RPG Maker music/sounds/sprites,
as well as stuff from other places.

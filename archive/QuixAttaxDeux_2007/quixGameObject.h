class quixGameObject {
  private:
  bool enabled;
  bool fullscreen;
  public:
  void changeScreenSize(int);
  BITMAP *bmpBackground;
  bool getEnabled();
  void setEnabled(bool);
  bool getFullscreen();
  void setFullscreen(bool);
  quixGameObject() { fullscreen = true; }
};

class MenuObject: public quixGameObject {
  private:
  bool done;
  int controlScheme[4];
  int fragLimit;
  int barrierScheme;
  public:
  BITMAP *bmpWindow;
  BITMAP *bmpPlay;
  BITMAP *bmpPlayDepress;
  BITMAP *bmpExit;
  BITMAP *bmpExitDepress;
  BITMAP *bmpCursor;
  BITMAP *bmpControlScheme;
  BITMAP *bmpArrows;
  BITMAP *bmpScreenOpts;
  bool isDone();
  bool ExitPressed;
  bool PlayPressed;
  bool mouseDown;
  bool resetPressed;
  bool fragLimitHit;
  void quit();
  int returnControlScheme(int);
  void setControlScheme(int, int);
  int CheckClick(int, int);
  int xPlay, yPlay, xExit, yExit;
  MenuObject();
};

class LevelObject: public quixGameObject {
  private:
  int iFragLimit;
  int iBarrierScheme;
  int frame;
  int barrierX[20];
  int barrierY[20];
  int amountOfBarriers;
  int powerupType;
  int powerupFrame;
  int powerupX, powerupY;
  public:
  LevelObject();
  BITMAP *bmpPlayerFilmstrip;
  BITMAP *bmpLaserFilmstrip;

  int getFragLimit();
  void setFragLimit(int);
  int getBarrierScheme();
  void setBarrierScheme(int);
  int getFrame();
  void setFrame(int);

  int getBarrierX(int);
  int getBarrierY(int);
  void setBarrierCoordinates(int, int, int);

  int getBarrierAmount();
  void setBarrierAmount(int);

  void createPowerup();

  int timeForAPowerupTimer;
  //~LevelObject();
};
